# -*- coding: utf-8 -*-
from __future__ import print_function
import os
import contextlib
import zipfile
try:
    from StringIO import StringIO, StringIO as outIO
except ImportError:
    from io import StringIO, BytesIO as outIO
import argparse
import csv
import numpy as np
from scipy.interpolate import interp1d
from scipy.optimize import brentq
from scipy.special import sici, hyp1f2, gamma as gammaf
from timeit import default_timer as timer


def MakeParser():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-f', '--filename', type=str, default='',
                        help="Loading object's data from file")
    parser.add_argument('-g', '--gamma', type=float, default=0.5,
                        help="Parameter gamma of the model core")
    parser.add_argument('-r', '--rho', action='store_true',
                        help="If set, rho will be selected as independent variable. Default independent variable is 't'")
    parser.add_argument('--rholim', type=float, default=[10, 1000], nargs=2,
                        help="Tuple, optional (Min, Max) values of 'rho'")
    parser.add_argument('--tlim', type=float, default=[10, 1000], nargs=2,
                        help="Tuple, optional (Min, Max) values of 't'")
    parser.add_argument('--nx_pp', type=int, default=20,
                        help="Number of points in independent variable numerical grid (log scale) per power if var == 't'")
    parser.add_argument('--slim', type=float, default=[0.1, 10], nargs=2,
                        help="Tuple, optional (Min, Max) values of 's'")
    parser.add_argument('--ds', type=float, default=0.02,
                        help="Step of the numerical grid over 's'")
    parser.add_argument('--log_s', action='store_true',
                        help="Use logarithmic numerical grid for 's'")
    parser.add_argument('--ns_pp', type=int, default=50,
                        help="int, optional. Number of points in 's' numerical grid per power in case of --log_s is specified.")
    parser.add_argument('--p_powlim', type=int, default=[-16, 2], nargs=2,
                        help="Tuple, optional. (Min, Max) values of the power of p. Define the limits of integration by p: (10^Min, 10^Max)")
    parser.add_argument('--n_pp', type=int, default=500,
                        help="int, optional. Number of points in p numerical grid (log scale) per power.\n" +
                             "E.g., if p_powlim = (-8, 2) the total number of points in p grid is n_pp * 10")
    parser.add_argument('--use_gpu', action='store_true',
                        help="Compute integrals on GPU")
    parser.add_argument('--cl_platform_id', type=int, default=-1,
                        help="int, optional. OpenCL platfrom ID")
    parser.add_argument('--cl_device_id', type=int, default=-1,
                        help="int, optional. OpenCL device ID in specified platform")
    parser.add_argument('--float64', action='store_true',
                        help="Calculate with double precision on GPU")
    return parser


class SelfSim:
    """Self-similarity exploration for transport equation with model 1D core
    See Sec. 3 in A.B. Kukushkin and P.A. Sdvizhenskii, J. Phys. A: Math. Theor. 49 (2016) 255002, doi:10.1088/1751-8113/49/25/255002"""

    def __init__(self, filename='', gamma=0.5, rho=False, rholim=(10, 1000), nx_pp=20, tlim=(10, 1000),
                 slim=(0.1, 10), ds=0.02, log_s=False, ns_pp=50, p_powlim=(-16, 2), n_pp=500,
                 use_gpu=False, cl_platform_id=-1, cl_device_id=-1, float64=False):
        """ filename : string, optional
                Loading object's data from file.
            gamma : float, optional
                Parameter \gamma of the model core (see Eq. 18 in J. Phys. A)
            rho : bool, optional
                If true, rho will be selected as independent variable. Default independent variable is t.
            rholim : tuple, optional
                (Min, Max) values of \rho
            nx_pp : float, optional
                Number of points in numerical grid (log scale) of independent varialbe (t or rho) per power
            tlim : tuple, optional
                (Min, Max) values of t
            slim : tuple, optional
                (Min, Max) values of s
            ds : float, optional
                Step of the s numerical grid
            log_s : bool, optional
                Use logarithmic numerical grid for 's'
            ns_pp : int, optional
                Number of points in 's' numerical grid per power in case of log_s == True.
            p_powlim : tuple, optional
                (Min, Max) values of the power of p. Define the limits of integration by p: (10^Min, 10^Max)
            n_pp : int, optional
                Number of points in p numerical grid (log scale) per power. E.g., if p_powlim = (-8, 2) the total number of points
                in p grid is n_pp * 10
            use_gpu : bool, optional
                Use OpenCL-compatible GPU to accelerate claculations
            cl_platform_id : int, optional
                Index of the OpenCL platform to use. -1 for auto selection
            cl_platform_id : int, optional
                Index of the OpenCL device to use (in the specified platform). -1 for auto selection"""
        self.N_MAX = 536870912  # 4GB limit for any mesh (double)
        self.N_COS = 512
        self.N_EXP = 8192
        self.XLIM_EXP = 10 * np.log(10)  # max value for t * q(p) for error ~ 1.e-7
        self.x = None  # t numerical grid (or list of grids if var == 'rho')
        self.nx = None  # number of points in t grid
        self.s = None  # list of s numerical grids
        self.q_inner = None  # inner integral array
        self.es = None  # list of exact solution arrays
        self.auts = None  # list of automodel solution arrays
        self.roots = None  # list of self-similar functions, Q_W(s)
        self.root0 = None  # t(rho)-averaged root (2-column: s, value(s))
        self.self_dir = os.path.dirname(os.path.realpath(__file__))
        success = self.load(filename)
        if not success:
            self.gamma = gamma
            self.rho = rho
            self.nx_pp = nx_pp
            self.tlim = tlim
            self.rholim = rholim
            self.slim = slim
            self.log_s = log_s
            self.ns_pp = ns_pp
            self.ds = ds
            self.p_powlim = p_powlim
            self.n_pp = n_pp
        self.use_gpu = use_gpu  # override the value obtained from load()
        self.cl_platform_id = cl_platform_id
        self.cl_device_id = cl_device_id
        self.float64 = float64
        self.xlim = {'t': self.tlim, 'rho': self.rholim}
        self.v = ('rho', 't') if self.rho else ('t', 'rho')
        self.np = int(round(self.n_pp * (self.p_powlim[1] - self.p_powlim[0]))) + 1
        self.p = np.logspace(self.p_powlim[0], self.p_powlim[1], self.np)
        self.dp_pow = float(self.p_powlim[1] - self.p_powlim[0]) / self.np
        if 1. - 1.e-6 < self.gamma < 1. + 1.e-6:
            self.coeff_small_p = 0.5 * np.pi
        else:
            self.coeff_small_p = np.cos(0.5 * np.pi * self.gamma) * gammaf(1. - self.gamma)
        self.create_meshes()
        if self.use_gpu:
            self._cl_init()

    def _cl_init(self):
        """Initializes OpenCL: creates the context, builds the program"""
        if not self.use_gpu:
            return
        from opencl_utils import device_select
        import pyopencl as cl
        device, gflops = device_select(self.cl_platform_id, self.cl_device_id)
        self.cl_context = cl.Context([device])
        self.cl_block_size = 256
        kernel_source_file = os.path.join(self.self_dir, 'SelfSimKernels.cl')
        compile_options = '-DBlockSize=%d -DN_COS=%d -DN_EXP=%d -DN_MAX=%d' % (self.cl_block_size, self.N_COS, self.N_EXP, self.N_MAX)
        if self.float64:
            compile_options += ' -DFP=double'
        self.cl_prog = cl.Program(self.cl_context, open(kernel_source_file).read()).build(options=compile_options)

    def create_meshes(self):
        """Creates numerical grids"""
        powlim_up = np.log10(self.xlim[self.v[0]][1])
        powlim_down = np.log10(self.xlim[self.v[0]][0])
        self.nx = int(round((powlim_up - powlim_down) * self.nx_pp)) + 1
        self.x = {}
        self.x[self.v[0]] = np.logspace(powlim_down, powlim_up, self.nx)
        if self.rho:
            s_max = ((self.xlim['t'][1] + 1.) ** (1. / self.gamma) - 1.) / self.x['rho']
            s_min = ((self.xlim['t'][0] + 1.) ** (1. / self.gamma) - 1.) / self.x['rho']
        else:
            s_max = ((self.x['t'] + 1) ** (1. / self.gamma) - 1.) / self.xlim['rho'][0]
            s_min = ((self.x['t'] + 1) ** (1. / self.gamma) - 1.) / self.xlim['rho'][1]
        s_max[np.where(s_max > self.slim[1])] = self.slim[1]
        s_min[np.where(s_min < self.slim[0])] = self.slim[0]
        self.s = []
        self.x[self.v[1]] = []
        ind2del = []
        for i in range(self.nx):
            scurr = self._create_mesh_s(s_min[i], s_max[i])
            if not scurr.size:
                ind2del.append(i)
                continue
            self.s.append(scurr)
            if self.rho:
                self.x['t'].append((self.x['rho'][i] * scurr + 1.) ** (self.gamma) - 1.)
            else:
                self.x['rho'].append(((self.x['t'][i] + 1.) ** (1. / self.gamma) - 1.) / scurr)
        self.x[self.v[0]] = np.delete(self.x[self.v[0]], ind2del)
        self.nx = self.x[self.v[0]].size

    def _create_mesh_s(self, smin, smax):
        """Creates numerical grid for s in the range (smin, smax)"""
        if smax < smin:
            return np.array([])
        if self.log_s:
            s_powlim_up = np.log10(smax)
            s_powlim_down = np.log10(smin)
            ns = int(round((s_powlim_up - s_powlim_down) * self.ns_pp)) + 1
            return np.logspace(s_powlim_down, s_powlim_up, ns)

        return np.linspace(smin, smax, int(round((smax - smin) / self.ds)) + 1)

    def load(self, filename):
        """ Loads all parameters and calculated values from a zipfile.
            Return True, if successful.
            filename : str
                Path to a zipfile"""
        if not len(filename):
            return False
        if not zipfile.is_zipfile(filename):
            print("Error: %s is not a zipfile" % filename)
            return False
        with contextlib.closing(zipfile.ZipFile(filename)) as zf:
            namelist = zf.namelist()
            if 'kwargs.csv' in namelist:
                f = zf.open('kwargs.csv')
                for key, val in csv.reader(f):
                    setattr(self, key, eval(val))
            else:
                print("Error: kwargs are not in a zipfile" % filename)
                return False
            if 'q_inner.txt' in namelist:
                f = zf.open('q_inner.txt')
                self.q_inner = np.loadtxt(f)[:, 1]
            if 'exact_sol.txt' in namelist:
                f = zf.open('exact_sol.txt')
                self.es = [np.fromstring(line, sep=' ') for line in f]
            if 'auto_sol.txt' in namelist:
                f = zf.open('auto_sol.txt')
                self.auts = [np.fromstring(line, sep=' ') for line in f]
            if 'roots.txt' in namelist:
                f = zf.open('roots.txt')
                self.roots = [np.fromstring(line, sep=' ') for line in f]
            if 'root0.txt' in namelist:
                f = zf.open('root0.txt')
                self.root0 = np.loadtxt(f)

            return True

    def save(self, dir='', suffix='', extended=False, fmt='%.10e'):
        """ Saves all parameters and calculated values to a zipfile.
            dir : str, optional
                Directory where to save the file.
            suffix : str, optional
                Adds this string to the filename."""
        if len(suffix):
            suffix = '_' + suffix
        if len(dir) and not os.path.isdir(dir):
            os.makedirs(dir)
        filename = os.path.join(dir, 'selfsim_%s_%g-%g_gamma_%g%s.zip' % (self.v[0], self.xlim[self.v[0]][0],
                                                                          self.xlim[self.v[0]][1], self.gamma, suffix))
        with contextlib.closing(zipfile.ZipFile(filename, 'w', compression=zipfile.ZIP_DEFLATED)) as zf:
            kwargs = self._params2dict()
            out = StringIO()
            csvw = csv.writer(out)
            for key in kwargs.keys():
                csvw.writerow([key, kwargs[key]])
            zf.writestr('kwargs.csv', out.getvalue())
            if self.q_inner is not None:
                out = outIO()
                np.savetxt(out, np.array([self.p, self.q_inner]).T, fmt=fmt)
                zf.writestr('q_inner.txt', out.getvalue())
            if self.es is not None:
                self._save_list(zf, self.es, 'exact_sol', fmt=fmt)
            if self.auts is not None:
                self._save_list(zf, self.auts, 'auto_sol', fmt=fmt)
            if self.roots is not None:
                self._save_list(zf, self.roots, 'roots', fmt=fmt)
            if self.root0 is not None:
                out = outIO()
                np.savetxt(out, self.root0, fmt=fmt)
                zf.writestr('root0.txt', out.getvalue())
            if not extended:
                return
            if self.s is not None:
                self._save_list(zf, self.s, 's', fmt=fmt)
            if self.x is not None:
                out = outIO()
                np.savetxt(out, self.x[self.v[0]], fmt=fmt)
                zf.writestr('%s.txt' % self.v[0], out.getvalue())
                self._save_list(zf, self.x[self.v[1]], self.v[1], fmt=fmt)

    def _save_list(self, zf, x, name, fmt='%.10e'):
        """Saves the list of arrays 'x' in the zipfile object 'zf' under the name 'name'.txt"""
        out = outIO()
        for el in x:
            np.savetxt(out, el.reshape(1, el.shape[0]), fmt=fmt)
        zf.writestr('%s.txt' % name, out.getvalue())

    def _params2dict(self):
        """converts arguments into dict"""
        kwargs = {}
        kwargs['gamma'] = self.gamma
        kwargs['rho'] = self.rho
        kwargs['rholim'] = self.rholim
        kwargs['nx_pp'] = self.nx_pp
        kwargs['tlim'] = self.tlim
        kwargs['slim'] = self.slim
        kwargs['ds'] = self.ds
        kwargs['log_s'] = self.log_s
        kwargs['ns_pp'] = self.ns_pp
        kwargs['p_powlim'] = self.p_powlim
        kwargs['n_pp'] = self.n_pp
        kwargs['use_gpu'] = self.use_gpu
        kwargs['cl_platform_id'] = self.cl_platform_id
        kwargs['cl_device_id'] = self.cl_device_id
        kwargs['float64'] = self.float64

        return kwargs

    def _tail_int(self, p, x, g):
        """Computes the tail of the inner integral for small p"""
        if 1. - 1e-6 < g < 1. + 1e-6:
            return p**g * (0.5 * np.pi - sici(x)[0])
        if 2. - 1e-6 < g < 2. + 1e-6:
            return p**g * (np.sin(x) / x - sici(x)[1])
        if 3. - 1e-6 < g < 3. + 1e-6:
            return p**g * (0.5 * sici(x)[0] - 0.25 * np.pi + 0.5 * (np.sin(x) + x * np.cos(x)) / x**2)
        if 4. - 1e-6 < g < 4. + 1e-6:
            return p**g * (sici(x)[1] / 6. + (x * np.cos(x) - (x**2 - 2.) * np.sin(x)) / (6. * x**3))
        hyp1f2res = hyp1f2(1. - 0.5 * g, 1.5, 2. - 0.5 * g, - 0.25 * x ** 2)

        return p**g * (x ** (2. - g) * hyp1f2res[0] / (g - 2.) + np.cos(0.5 * np.pi * g) * gammaf(1. - g))

    def _main_int(self, ip0, xcut):
        """Computes the main part of the inner integral for small p"""
        ans = np.zeros(self.np)
        x = np.logspace(-6, 4, 200192)
        a = 1. / (1. + x) ** (self.gamma + 2.)
        dx = x[1:] - x[:-1]
        for ip in range(ip0, self.np):
            f = np.sin(self.p[ip] * x) * a
            ans[ip] = 1. - self.gamma * (1. + self.gamma) * 0.5 * ((f[1:] + f[:-1]) * dx).sum() / self.p[ip]
        x, dx = np.linspace(0, xcut, 100096, retstep=True)
        a = 1. / (1. + x) ** self.gamma
        for ip in range(ip0):
            f = np.sin(self.p[ip] * x) * a
            ans[ip] = self.p[ip] * dx * (f[1:-1].sum() + 0.5 * (f[-1] + f[0]))

        return ans

    def _main_int_cl(self, ip0, xcut):
        """Computes the main part of the inner integral for small p on GPU"""
        if not self.use_gpu:
            return None
        import pyopencl as cl
        mf = cl.mem_flags
        dtype = np.float64 if self.float64 else np.float32
        p_host = self.p if self.float64 else self.p.astype(np.float32)
        p_device = cl.Buffer(self.cl_context, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=p_host)
        res_host = np.zeros(self.np, dtype=dtype)
        res_device = cl.Buffer(self.cl_context, mf.WRITE_ONLY, res_host.nbytes)
        queue = cl.CommandQueue(self.cl_context)
        local_work_size = (self.cl_block_size, 1)
        nblocks = 4096 if np.float64 else 8192
        ip_st = ip0
        while ip_st < self.np:  # iterations to avoid kernel execution timeout
            global_work_size_lp = (min(nblocks, self.np - ip_st) * self.cl_block_size, 1)
            self.cl_prog.inner_int_large_p(queue, global_work_size_lp, local_work_size, res_device, p_device,
                                           np.uint32(ip_st), dtype(self.gamma + 2.), dtype(-6.), dtype(4.), np.uint32(200192))
            queue.finish()
            ip_st += nblocks
        ip_st = 0
        while ip_st < ip0:  # iterations to avoid kernel execution timeout
            global_work_size_sp = (min(nblocks, ip0 - ip_st) * self.cl_block_size, 1)
            self.cl_prog.inner_int_small_p(queue, global_work_size_sp, local_work_size, res_device, p_device,
                                           np.uint32(ip_st), dtype(self.gamma), dtype(xcut), np.uint32(100096))
            queue.finish()
            ip_st += nblocks
        cl.enqueue_copy(queue, res_host, res_device)
        p_device.release()
        res_device.release()
        ans = res_host if self.float64 else res_host.astype(np.float64)
        ans[ip0:] = 1. - self.gamma * (self.gamma + 1) * ans[ip0:] / self.p[ip0:]

        return ans

    def inner_int(self):
        """ Computes the inner integral in Eq. 18 in J. Phys. A.
            Returns the result."""
        start_inner_int = timer()
        xcut = 1000.
        if 1. - 1e-6 < self.gamma < 1. + 1e-6:
            sici_p = sici(self.p)
            add = sici_p[1] * np.sin(self.p) - sici_p[0] * np.cos(self.p)
            self.q_inner = self.p * (0.5 * np.pi * np.cos(self.p) + add)
        else:
            px_lim = self.p * xcut
            ip0 = np.argmax(px_lim > 15)
            if self.use_gpu:
                self.q_inner = self._main_int_cl(ip0, xcut)
            else:
                self.q_inner = self._main_int(ip0, xcut)
            self.q_inner[:ip0] += self._tail_int(self.p[:ip0], px_lim[:ip0], self.gamma) - \
                self.gamma * self._tail_int(self.p[:ip0], px_lim[:ip0], self.gamma + 1.) + \
                0.5 * self.gamma * (self.gamma + 1.) * self._tail_int(self.p[:ip0], px_lim[:ip0], self.gamma + 2.)
        stop_inner_int = timer()
        print('inner_int() took: %g s' % (stop_inner_int - start_inner_int))

        return self.q_inner

    def exact_sol(self):
        """ Computes the exact solutions. Stores the results as a list of arrays in self.es.
            Returns self.es"""
        if self.q_inner is None:
            self.inner_int()
        start_exact_sol = timer()
        if self.use_gpu:
            self.es = self._integrate_exact_sol_cl()
        else:
            self.es = []
            q_inner_interp = interp1d(self.p, self.q_inner)
            if self.rho:
                dp_cos = 0.5 * np.pi / (self.x['rho'] * self.N_COS)
                for i in range(self.nx):
                    exp_lim = self.XLIM_EXP / self.x['t'][i]
                    res = np.zeros(self.x['t'][i].size)
                    for j in range(self.x['t'][i].size):
                        ip0 = np.argmax(self.q_inner > exp_lim[j])
                        p_lim = self.p[ip0] if ip0 else self.p[-1]
                        dp_exp = p_lim / self.N_EXP
                        dp = min(dp_exp, dp_cos[i])
                        res[j] = self._integrate_exact_sol(self.x['rho'][i], self.x['t'][i][j], q_inner_interp, p_lim, dp)
                    self.es.append(res)
            else:
                exp_lim = self.XLIM_EXP / self.x['t']
                for i in range(self.nx):
                    ip0 = np.argmax(self.q_inner > exp_lim[i])
                    p_lim = self.p[ip0] if ip0 else self.p[-1]
                    dp_exp = p_lim / self.N_EXP
                    dp_cos = 0.5 * np.pi / (self.x['rho'][i] * self.N_COS)
                    res = np.zeros(self.x['rho'][i].size)
                    for j in range(self.x['rho'][i].size):
                        dp = min(dp_exp, dp_cos[j])
                        res[j] = self._integrate_exact_sol(self.x['rho'][i][j], self.x['t'][i], q_inner_interp, p_lim, dp)
                    self.es.append(res)
        stop_exact_sol = timer()
        print('exact_sol() took: %g s' % (stop_exact_sol - start_exact_sol))

        return self.es

    def _integrate_exact_sol(self, rho, t, q_inner_interp, p_lim, dp):
        """ Computes the p-integral in the exact solution for given t and rho"""
        np_mesh = min(self.N_MAX, int(p_lim / dp) + 1)
        p1, dp1 = np.linspace(0, p_lim, np_mesh, retstep=True)
        ip0 = np.argmax(p1 > self.p[0])
        if not ip0:
            q = self.coeff_small_p * p1**self.gamma
        else:
            q = np.zeros(p1.size)
            q[:ip0] = self.coeff_small_p * p1[:ip0]**self.gamma
            q[ip0:] = q_inner_interp(p1[ip0:])
        f = np.cos(rho * p1) * np.exp(- t * q)

        return (f[1:-1].sum() + 0.5 * (f[0] + f[-1])) * dp1 / np.pi

    def _integrate_exact_sol_cl(self, nqueues=3):
        """ Computes the p-integral in the exact solution on GPU"""
        if not self.use_gpu:
            return None
        import pyopencl as cl
        es = []
        if self.rho:
            dp_cos = 0.5 * np.pi / (self.x['rho'] * self.N_COS)
        else:
            exp_lim = self.XLIM_EXP / self.x['t']
        mf = cl.mem_flags
        dtype = np.float64 if self.float64 else np.float32
        byte1 = 8 if self.float64 else 4
        q_host = self.q_inner if self.float64 else self.q_inner.astype(np.float32)
        q_device = cl.Buffer(self.cl_context, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=q_host)
        nbuff = 0
        for i in range(self.nx):
            nbuff = max(self.s[i].size, nbuff)
        queue = []
        x_device = []
        res_device = []
        res_host = []
        plim_device = []
        for i in range(nqueues):
            queue.append(cl.CommandQueue(self.cl_context))
            x_device.append(cl.Buffer(self.cl_context, mf.READ_ONLY, nbuff * byte1))
            res_device.append(cl.Buffer(self.cl_context, mf.WRITE_ONLY, nbuff * byte1))
            res_host.append(np.zeros(nbuff, dtype=dtype))
            if self.rho:
                plim_device.append(cl.Buffer(self.cl_context, mf.READ_ONLY, nbuff * byte1))
        local_work_size = (self.cl_block_size, 1)
        for i in range(self.nx):
            iq = i % nqueues
            if i > iq:
                cl.enqueue_copy(queue[iq], res_host[iq][:self.s[i - nqueues].size], res_device[iq])
                if self.float64:
                    es.append(res_host[iq][:self.s[i - nqueues].size] / np.pi)
                else:
                    es.append(res_host[iq][:self.s[i - nqueues].size].astype(np.float64) / np.pi)
            x_host = self.x[self.v[1]][i] if self.float64 else self.x[self.v[1]][i].astype(np.float32)
            cl.enqueue_copy(queue[iq], x_device[iq], x_host)
            global_work_size = (self.s[i].size * self.cl_block_size, 1)
            if self.rho:
                exp_lim = self.XLIM_EXP / self.x['t'][i]
                plim_host = np.zeros(self.x['t'][i].size, dtype=dtype)
                for j in range(self.x['t'][i].size):
                    ip0 = np.argmax(self.q_inner > exp_lim[j])
                    plim_host[j] = dtype(self.p[ip0]) if ip0 else dtype(self.p[-1])
                cl.enqueue_copy(queue[iq], plim_device[iq], plim_host)
                self.cl_prog.exact_sol_rho_int(queue[iq], global_work_size, local_work_size,
                                               res_device[iq], q_device, x_device[iq], plim_device[iq],
                                               dtype(self.x['rho'][i]), dtype(dp_cos[i]), dtype(self.p_powlim[0]),
                                               dtype(self.dp_pow), dtype(self.gamma), dtype(self.coeff_small_p))
            else:
                ip0 = np.argmax(self.q_inner > exp_lim[i])
                p_lim = self.p[ip0] if ip0 else self.p[-1]
                dp_exp = p_lim / self.N_EXP
                self.cl_prog.exact_sol_t_int(queue[iq], global_work_size, local_work_size,
                                             res_device[iq], q_device, x_device[iq],
                                             dtype(self.x['t'][i]), dtype(p_lim), dtype(dp_exp), dtype(self.p_powlim[0]),
                                             dtype(self.dp_pow), dtype(self.gamma), dtype(self.coeff_small_p))
            queue[iq].flush()
        for i in range(self.nx - nqueues, self.nx):
            iq = i % nqueues
            cl.enqueue_copy(queue[iq], res_host[iq][:self.s[i].size], res_device[iq])
            if self.float64:
                es.append(res_host[iq][:self.s[i].size] / np.pi)
            else:
                es.append(res_host[iq][:self.s[i].size].astype(np.float64) / np.pi)
            x_device[iq].release()
            res_device[iq].release()
            if self.rho:
                plim_device[iq].release()
        q_device.release()

        return es

    def automodel_sol(self):
        """ Computes automodel solutions. Stores the results as a list of arrays in self.auts.
            Returns self.auts"""
        if self.roots is None:
            self.root()
        root0i = interp1d(self.root0[:, 0], self.root0[:, 1], bounds_error=False, fill_value=(self.root0[0, 1], self.root0[-1, 1]))
        # g = [(self.roots[-1] - self.roots[0]) / (np.log10(self.x[self.v[0]][-1])**(-1.55) - np.log10(self.x[self.v[0]][0])**(-1.55)) \
        # * (np.log10(self.x[self.v[0]][i])**(-1.55) - np.log10(self.x[self.v[0]][0])**(-1.55)) + self.roots[0] for i in range(self.nx)]
        self.auts = [0.5 * self.x['t'][i] * self.gamma / (1. + self.x['rho'][i] * root0i(self.s[i])) ** (1. + self.gamma) for i in range(self.nx)]
        # self.auts = [0.5 * self.x['t'][i] * self.gamma / (1. + self.x['rho'][i] * g[i]) ** (1. + self.gamma) for i in range(self.nx)]

        return self.auts

    def root(self, limits=(0.0001, 100000.)):
        """ Solves the equation to find the self-similar functions. Stores the results as a list of arrays in self.roots.
            Returns self.roots
            limits : tuple, optional
                (Min, Max) values of the possible roots of the equation (and of the self-similar function)"""
        if self.es is None:
            self.exact_sol()
        start_root = timer()
        self.roots = []
        for i in range(len(self.es)):
            x = np.zeros(self.es[i].size)
            for j in range(self.es[i].size):
                if self.rho:
                    def f(x):
                        return 0.5 * self.gamma * self.x['t'][i][j] / (1. + self.x['rho'][i] * x) ** (self.gamma + 1.) - self.es[i][j]
                else:
                    def f(x):
                        return 0.5 * self.gamma * self.x['t'][i] / (1. + self.x['rho'][i][j] * x) ** (self.gamma + 1.) - self.es[i][j]
                x[j] = brentq(f, limits[0], limits[1])
            self.roots.append(x)
        stop_root = timer()
        self.root0 = self._calc_root0()
        print('Root() took: %g s' % (stop_root - start_root))

        return self.roots

    def _calc_root0(self):
        """Computes g(s) for automodel solution averaged over independent variable"""
        x = self.x[self.v[0]]
        smin = 1e9
        smax = 0
        for i in range(x.size):
            smin = min(smin, self.s[i][0])
            smax = max(smax, self.s[i][-1])
        s0 = self._create_mesh_s(smin + 1.e-10, smax - 1.e-10)
        root0 = np.zeros((s0.size, 2))
        root0[:, 0] = s0
        weights = np.zeros(s0.size)
        weight_x = np.zeros(x.size)
        weight_x[1:-1] = 0.5 * (x[2:] - x[:-2])
        weight_x[0] = 0.5 * (x[1] - x[0])
        weight_x[-1] = 0.5 * (x[-1] - x[-2])
        for i in range(x.size):
            if self.s[i].size > 1:
                rooti = interp1d(self.s[i], self.roots[i], bounds_error=False, fill_value=0)(s0)
                root0[:, 1] += weight_x[i] * rooti
                weights += weight_x[i] * (rooti > 0)
        root0[:, 1] /= weights

        return root0

    def plot_inner_int(self, interactive=True, save=False, dir='', suffix='', fmt='png', dpi=300):
        """ Plots the inner integral in Eq. (18) in J. Phys. A as a function of p.
            Returns matplotlib figure
            interactive : bool, optional
                If true, creates interactive figure. Set to False for servers without X.
            save : bool, optional
                Save the image or not.
            dir : str, optional
                Directory where to save the image.
            suffix : str, optional
                Adds this string to image filename.
            format : str, optional
                Image format: png, jpg, eps, ...
            dpi : int, optional
                Dots per inch for saved image."""
        if len(suffix):
            suffix = '_' + suffix
        if not interactive:
            import matplotlib as mpl
            mpl.use('Agg')
            save = True
        from matplotlib import pyplot as plt
        if self.q_inner is None:
            self.q_inner = self.inner_int()
        fig = plt.figure(figsize=(5., 4.), facecolor='w')
        ax = fig.add_axes([0.14, 0.13, 0.84, 0.85])
        ax.plot(self.p, self.q_inner, zorder=2)
        ax.plot([self.p[0], self.p[-1]], [1., 1.], color='0.6', ls='--', zorder=1)
        ax.text(0.95, 0.05, '$\gamma$ = %g' % self.gamma, ha='right', transform=ax.transAxes)
        ax.set_xscale('log')
        ax.set_yscale('log')
        ax.set_xlabel('p')
        ax.set_ylabel('Q$_{inner}$')
        if save:
            if len(dir) and not os.path.isdir(dir):
                os.makedirs(dir)
            fig.savefig(os.path.join('%s' % dir, 'inner_int_gamma_%g%s.%s' % (self.gamma, suffix, fmt)), dpi=dpi)

        return fig

    def plot_es(self, value_list, interactive=True, save=False, dir='', suffix='', fmt='png', dpi=300):
        """ Plots the exact solutions for the given values of \rho (if var == 'rho') or t (if var == 't') as functions of s.
            Returns matplotlib figure
            value_list : iterable
                List of \rho (or t) values within the rholim (or tlim) interval for which the exact solutions will be plotted
            interactive : bool, optional
                If true, creates interactive figure. Set to False for servers without X.
            save : bool, optional
                Save the image or not.
            dir : str, optional
                Directory where to save the image.
            suffix : str, optional
                Adds this string to image filename.
            format : str, optional
                Image format: png, jpg, eps, ...
            dpi : int, optional
                Dots per inch for saved image."""
        if len(suffix):
            suffix = '_' + suffix
        if not interactive:
            import matplotlib as mpl
            mpl.use('Agg')
            save = True
        from matplotlib import pyplot as plt
        x = self.x[self.v[0]]
        if self.es is None:
            self.exact_sol()
        fig = plt.figure(figsize=(5., 4.), facecolor='w')
        ax = fig.add_axes([0.14, 0.13, 0.84, 0.8])
        iclist = []
        for val in value_list:
            if not x[0] <= val <= x[-1]:
                print('Plotting warning: value %g is out of the range (%g, %g)' % (val, x[0], x[-1]))
                continue
            ic = np.argmin(np.abs(val - x))  # index of the closest value
            if ic in iclist:
                continue
            iclist.append(ic)
            ax.plot(self.s[ic], 10000 * self.es[ic], label='%g' % x[ic])
        ax.set_title('$\gamma$ = %g' % self.gamma)
        ax.legend(loc=0, frameon=False, ncol=len(value_list) // 4 + 1)
        if self.log_s:
            ax.set_xscale('log')
        ax.set_xlabel('s')
        ax.set_ylabel('f$_{exact}$, 10$^4$')
        if save:
            if len(dir) and not os.path.isdir(dir):
                os.makedirs(dir)
            fig.savefig(os.path.join('%s' % dir, 'exact_sol_%s_gamma_%g_%s%s.%s' % (self.v[0], self.gamma,
                                                                                    '_'.join(['%g' % val for val in value_list]),
                                                                                    suffix, fmt)), dpi=dpi)

        return fig

    def plot_roots_3d(self, step=1, xlim=(None, None), interactive=True, save=False, dir='', suffix='', fmt='png', dpi=300):
        """ Plots the calculated self similar functions (roots) in the 3D space.
            Returns matplotlib figure
            step : int, optional
                If specified, only each i-th == step self similar function will be plotted
            xlim : tuple of two floats, optional
                Plot roots only in the interval xlim[0] < rho (or t) < xlim[1]
            interactive : bool, optional
                If true, creates interactive figure. Set to False for servers without X.
            save : bool, optional
                Save the image or not.
            dir : str, optional
                Directory where to save the image.
            suffix : str, optional
                Adds this string to image filename.
            format : str, optional
                Image format: png, jpg, eps, ...
            dpi : int, optional
                Dots per inch for saved image."""
        if len(suffix):
            suffix = '_' + suffix
        if not interactive:
            import matplotlib as mpl
            mpl.use('Agg')
            save = True
        from matplotlib import pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D
        x = self.x[self.v[0]]
        if self.roots is None:
            self.root()
        fig = plt.figure(figsize=(7., 7.), facecolor='w')
        ax = fig.add_axes([0.05, 0.05, 0.85, 0.85], projection='3d')
        i0 = np.argmax(x >= xlim[0]) if xlim[0] is not None else 0
        i1 = np.argmin(x <= xlim[1]) if xlim[1] is not None else x.size - 1
        for i in range(i0, i1 + 1, max(1, step)):
            xp = x[i] * np.ones(self.s[i].size)
            ax.plot(xp, self.s[i], self.roots[i], '-k', lw=0.75)
        ax.set_xlabel(self.v[0])
        ax.set_ylabel('s')
        ax.set_zlabel('Q$_{W}$(s, %s)' % self.v[0])
        fig.text(0.9, 0.9, '$\gamma$ = %g' % self.gamma, ha='right', va='top')
        if save:
            if len(dir) and not os.path.isdir(dir):
                os.makedirs(dir)
            fig.savefig(os.path.join('%s' % dir, 'roots3d_%s_gamma_%g%s.%s' % (self.v[0], self.gamma, suffix, fmt)), dpi=dpi)

        return fig

    def plot_roots_s(self, value_list, interactive=True, save=False, dir='', suffix='', fmt='png', dpi=300):
        """ Plots roots for the given values of \rho (if var == 'rho') or t (if var == 't') as functions of s.
            Returns matplotlib figure
            value_list : iterable
                List of \rho (or t) values within the rholim (or tlim) interval for which the roots will be plotted
            interactive : bool, optional
                If true, creates interactive figure. Set to False for servers without X.
            save : bool, optional
                Save the image or not.
            dir : str, optional
                Directory where to save the image.
            suffix : str, optional
                Adds this string to image filename.
            format : str, optional
                Image format: png, jpg, eps, ...
            dpi : int, optional
                Dots per inch for saved image."""
        if len(suffix):
            suffix = '_' + suffix
        if not interactive:
            import matplotlib as mpl
            mpl.use('Agg')
            save = True
        from matplotlib import pyplot as plt
        x = self.x[self.v[0]]
        if self.roots is None:
            self.root()
        fig = plt.figure(figsize=(6., 4.), facecolor='w')
        ax = fig.add_axes([0.12, 0.13, 0.86, 0.8])
        ls = ['-', '--', ':', '-.']
        lw = [1.5, 1., 2., 1.]
        iclist = []
        for ival, val in enumerate(value_list):
            if not x[0] <= val <= x[-1]:
                print('Plotting warning: value %g is out of the range (%g, %g)' % (val, x[0], x[-1]))
                continue
            ic = np.argmin(np.abs(val - x))  # index of the closest value
            if ic in iclist:
                continue
            iclist.append(ic)
            ax.plot(self.s[ic], self.roots[ic], label='%g' % x[ic], ls=ls[ival % 4], lw=lw[ival % 4])
        ax.set_title('$\gamma$ = %g' % self.gamma)
        ax.legend(loc=0, frameon=False, ncol=len(value_list) // 6 + 1, title=self.v[0])
        if self.log_s:
            ax.set_xscale('log')
            ax.set_yscale('log')
        ax.set_xlabel('s')
        ax.set_ylabel('Q$_{W}$(s)')
        if save:
            if len(dir) and not os.path.isdir(dir):
                os.makedirs(dir)
            fig.savefig(os.path.join('%s' % dir, 'roots_s_%s_gamma_%g_%s%s.%s' % (self.v[0], self.gamma,
                                                                                  '_'.join(['%g' % val for val in value_list]),
                                                                                  suffix, fmt)), dpi=dpi)

        return fig

    def plot_roots_s_all(self, normalize=False, xlim=(None, None), interactive=True, save=False, dir='', suffix='', fmt='png', dpi=300):
        """ Plots all roots as functions of s.
            Returns matplotlib figure
            normalize : bool, optional
                If true, normalizes all roots to rho-average (or t-average) root.
            xlim : tuple of two floats, optional
                Plot roots only in the interval xlim[0] < rho (or t) < xlim[1]
            interactive : bool, optional
                If true, creates interactive figure. Set to False for servers without X.
            save : bool, optional
                Save the image or not.
            dir : str, optional
                Directory where to save the image.
            suffix : str, optional
                Adds this string to image filename.
            format : str, optional
                Image format: png, jpg, eps, ...
            dpi : int, optional
                Dots per inch for saved image."""
        if len(suffix):
            suffix = '_' + suffix
        if not interactive:
            import matplotlib as mpl
            mpl.use('Agg')
            save = True
        from matplotlib import pyplot as plt
        x = self.x[self.v[0]]
        if self.roots is None:
            self.root()
        fig = plt.figure(figsize=(6., 4.), facecolor='w')
        ax = fig.add_axes([0.13, 0.13, 0.85, 0.8])
        if normalize:
            root0i = interp1d(self.root0[:, 0], self.root0[:, 1], bounds_error=False, fill_value=(self.root0[0, 1], self.root0[-1, 1]))
            suffix = '_normalized' + suffix
            ylabel = 'Q$_{W}$(s, %s)/{Q$_{W}$}$_{av}$(s)' % self.v[0]
        else:
            ylabel = 'Q$_{W}$(s, %s)' % self.v[0]
        i0 = np.argmax(x >= xlim[0]) if xlim[0] is not None else 0
        i1 = np.argmin(x <= xlim[1]) if xlim[1] is not None else x.size - 1
        if normalize:
            l0, = ax.plot(self.s[i0], self.roots[i0] / root0i(self.s[i0]), ls='-', color='C0', lw=1.0, zorder=x.size + 1)
            l1, = ax.plot(self.s[i1], self.roots[i1] / root0i(self.s[i1]), ls='-', color='C1', lw=1.0, zorder=x.size + 2)
        else:
            l0, = ax.plot(self.s[i0], self.roots[i0], ls='-', color='C0', lw=1.0, zorder=x.size + 1)
            l1, = ax.plot(self.s[i1], self.roots[i1], ls='-', color='C1', lw=1.0, zorder=x.size + 2)
        for i in range(i0 + 1, i1):
            if normalize:
                ax.plot(self.s[i], self.roots[i] / root0i(self.s[i]), '-k', lw=0.5, alpha=0.25)
            else:
                ax.plot(self.s[i], self.roots[i], '-k', lw=0.5, alpha=0.25)
        ax.legend([l0, l1], ['%s = %g' % (self.v[0], x[i0]), '%s = %g' % (self.v[0], x[i1])], loc=0, frameon=False)
        ax.set_title('$\gamma$ = %g' % self.gamma)
        if self.log_s:
            ax.set_xscale('log')
            if not normalize:
                ax.set_yscale('log')
        ax.set_xlabel('s')
        ax.set_ylabel(ylabel)
        if save:
            if len(dir) and not os.path.isdir(dir):
                os.makedirs(dir)
            fig.savefig(os.path.join('%s' % dir, 'roots_s_all_%s_%g-%g_gamma_%g%s.%s' % (self.v[0], x[i0], x[i1],
                                                                                         self.gamma, suffix, fmt)), dpi=dpi)

        return fig

    def plot_automodel_err(self, xlim=(None, None), interactive=True, save=False, dir='', suffix='', fmt='png', dpi=300):
        """ Plots errors of the automodel solutions.
            Returns matplotlib figure
            xlim : tuple of two floats, optional
                Plot roots only in the interval xlim[0] < rho (or t) < xlim[1]
            interactive : bool, optional
                If true, creates interactive figure. Set to False for servers without X.
            save : bool, optional
                Save the image or not.
            dir : str, optional
                Directory where to save the image.
            suffix : str, optional
                Adds this string to image filename.
            format : str, optional
                Image format: png, jpg, eps, ...
            dpi : int, optional
                Dots per inch for saved image."""
        if len(suffix):
            suffix = '_' + suffix
        if not interactive:
            import matplotlib as mpl
            mpl.use('Agg')
            save = True
        from matplotlib import pyplot as plt
        x = self.x[self.v[0]]
        if self.auts is None:
            self.automodel_sol()
        fig = plt.figure(figsize=(6., 4.), facecolor='w')
        ax = fig.add_axes([0.13, 0.13, 0.85, 0.8])
        i0 = np.argmax(x >= xlim[0]) if xlim[0] is not None else 0
        i1 = np.argmin(x <= xlim[1]) if xlim[1] is not None else x.size - 1
        l0, = ax.plot(self.x[self.v[1]][i0], self.auts[i0] / self.es[i0], ls='-', color='C0', lw=1.0, zorder=x.size + 1)
        l1, = ax.plot(self.x[self.v[1]][i1], self.auts[i1] / self.es[i1], ls='-', color='C1', lw=1.0, zorder=x.size + 2)
        for i in range(i0 + 1, i1):
            ax.plot(self.x[self.v[1]][i], self.auts[i] / self.es[i], '-k', lw=0.5, alpha=0.25)
        ax.set_xlabel('$t$' if self.rho else r'$\rho$')
        ax.legend([l0, l1], ['%s = %g' % (self.v[0], x[i0]), '%s = %g' % (self.v[0], x[i1])], loc=0, frameon=False)
        ax.set_title('$\gamma$ = %g' % self.gamma)
        ax.set_xscale('log')
        ax.set_ylabel(r'$f_{auto}(\rho, t)$/$f_{exact}(\rho, t)$')
        if save:
            if len(dir) and not os.path.isdir(dir):
                os.makedirs(dir)
            fig.savefig(os.path.join('%s' % dir, 'auto_err_%s_%g-%g_gamma_%g%s.%s' % (self.v[0], x[i0], x[i1],
                                                                                      self.gamma, suffix, fmt)), dpi=dpi)

        return fig

    def plot_roots_proj(self, step=1, xnorm=None, xlim=(None, None), interactive=True, save=False, dir='', suffix='', fmt='png', dpi=300):
        """ Plots normalized self similar functions (roots) in the 2D projection.
            Returns matplotlib figure
            step : int, optional
                If specified, only each i-th == step self similar function will be plotted
            xnorm : float or None, optional
                If specified, all roots, Q(x,s), will be normalized to Q(xnorm,s).
                If not specified, Q(x,s) will be normalized to x-averaged Q_av(s).
            xlim : tuple of two floats, optional
                Plot roots only in the interval xlim[0] < rho (or t) < xlim[1]
            interactive : bool, optional
                If true, creates interactive figure. Set to False for servers without X.
            save : bool, optional
                Save the image or not.
            dir : str, optional
                Directory where to save the image.
            suffix : str, optional
                Adds this string to image filename.
            format : str, optional
                Image format: png, jpg, eps, ...
            dpi : int, optional
                Dots per inch for saved image."""
        if len(suffix):
            suffix = '_' + suffix
        if not interactive:
            import matplotlib as mpl
            mpl.use('Agg')
            save = True
        from matplotlib import pyplot as plt
        x = self.x[self.v[0]]
        if self.roots is None:
            self.root()
        if xnorm is None:
            ylabel = 'Q$_{W}$(s, %s)/{Q$_{W}$}$_{av}$(s)' % self.v[0]
            root0i = interp1d(self.root0[:, 0], self.root0[:, 1], bounds_error=False, fill_value=(self.root0[0, 1], self.root0[-1, 1]))
        else:
            smin = 1e9
            smax = 0
            for i in range(x.size):
                smin = min(smin, self.s[i][0])
                smax = max(smax, self.s[i][-1])
            ind_sorted = np.argsort(np.abs(x - xnorm))
            root0 = None
            x0 = None
            for i in ind_sorted:
                if abs(self.s[i][0] - smin) < 1.e-6 and abs(self.s[i][-1] - smax) < 1.e-6:
                    root0 = self.roots[i]
                    s0 = self.s[i]
                    x0 = x[i]
                    break
            if root0 is None:
                print('Error: none of the roots is defined in the (%.3f, %.3f) interval' % (smin, smax))
                return None
            ylabel = 'Q$_{W}$(s, %s)/Q$_{W}$(s, %s = %.1f)' % (self.v[0], self.v[0], x0)
            suffix = '_%snorm_%.1f' % (self.v[0], x0) + suffix
            root0i = interp1d(s0, root0, bounds_error=False, fill_value=(root0[0], root0[-1]))
        fig = plt.figure(figsize=(6., 4.), facecolor='w')
        ax = fig.add_axes([0.14, 0.13, 0.84, 0.8])
        i0 = np.argmax(x >= xlim[0]) if xlim[0] is not None else 0
        i1 = np.argmin(x <= xlim[1]) if xlim[1] is not None else x.size - 1
        for i in range(i0, i1 + 1, max(1, step)):
            root_norm = self.roots[i] / root0i(self.s[i])
            ax.scatter(np.ones(root_norm.size) * x[i], root_norm, s=0.75, c='k', alpha=0.35)
        ax.set_xscale('log')
        ax.set_xlabel(self.v[0])
        ax.set_ylabel(ylabel)
        ax.set_title('$\gamma$ = %g' % self.gamma)
        if save:
            if len(dir) and not os.path.isdir(dir):
                os.makedirs(dir)
            fig.savefig(os.path.join('%s' % dir, 'roots_proj_%s_%g-%g_gamma_%g%s.%s' % (self.v[0], x[i0], x[i1],
                                                                                        self.gamma, suffix, fmt)), dpi=dpi)

        return fig


if __name__ == "__main__":
    parser = MakeParser()
    args = parser.parse_args()
    kwargs = vars(args)
    if 'filename' not in kwargs:
        for key in kwargs.keys():
            print(key, kwargs[key])
    selfsim = SelfSim(**kwargs)
    suffix = ''
    # fig1 = selfsim.plot_inner_int()
    # fig2 = selfsim.plot_es([10, 50, 100],save = True, suffix = suffix)
    # fig3 = selfsim.plot_roots_3d(save = True, suffix = suffix)
    fig4 = selfsim.plot_roots_proj(save=True, suffix=suffix)
    fig5 = selfsim.plot_roots_s_all(save=True, suffix=suffix)
    fig6 = selfsim.plot_roots_s_all(normalize=True, save=True, suffix=suffix)
    fig7 = selfsim.plot_automodel_err(save=True, suffix=suffix)
    selfsim.save(extended=True, suffix=suffix)
    from matplotlib import pyplot as plt
    plt.show()

    quit()
