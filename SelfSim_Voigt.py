# -*- coding: utf-8 -*-
from __future__ import print_function
import os
import codecs
import contextlib
import zipfile
try:
    from StringIO import StringIO, StringIO as outIO
except ImportError:
    from io import StringIO, BytesIO as outIO
import argparse
import csv
import numpy as np
from scipy.interpolate import interp1d, CubicSpline
from scipy.optimize import brentq
from scipy.special import sici, hyp1f2, erfc, gamma as gammaf
from timeit import default_timer as timer


def MakeParser():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-f', '--filename', type=str, default='',
                        help="Loading object's data from file")
    parser.add_argument('-g', '--gamma', type=float, default=0.5,
                        help="Parameter gamma of the model core")
    parser.add_argument('-r', '--rho', action='store_true',
                        help="If set, rho will be selected as independent variable. Default independent variable is 't'")
    parser.add_argument('--rholim', type=float, default=[1.e-30, 1.e30], nargs=2,
                        help="Tuple, optional (Min, Max) values of 'rho'")
    parser.add_argument('--tlim', type=float, default=[30, 1.e8], nargs=2,
                        help="Tuple, optional (Min, Max) values of 't'")
    parser.add_argument('--nx_pp', type=int, default=50,
                        help="Number of points in independent variable numerical grid (log scale) per power if var == 't'")
    parser.add_argument('--slim', type=float, default=[0.01, 1.e3], nargs=2,
                        help="Tuple, optional (Min, Max) values of 's'")
    parser.add_argument('--ds', type=float, default=0.02,
                        help="Step of the numerical grid over 's'")
    parser.add_argument('--log_s', action='store_true',
                        help="Use logarithmic numerical grid for 's'")
    parser.add_argument('--ns_pp', type=int, default=50,
                        help="int, optional. Number of points in 's' numerical grid per power in case of --log_s is specified.")
    parser.add_argument('--p_powlim', type=int, default=[-16, 2], nargs=2,
                        help="Tuple, optional. (Min, Max) values of the power of p. Define the limits of integration by p: (10^Min, 10^Max)")
    parser.add_argument('--n_pp', type=int, default=2000,
                        help="int, optional. Number of points in p numerical grid (log scale) per power.\n" +
                             "E.g., if p_powlim = (-8, 2) the total number of points in p grid is n_pp * 10")
    parser.add_argument('--use_gpu', action='store_true',
                        help="Compute integrals on GPU")
    parser.add_argument('--cl_platform_id', type=int, default=-1,
                        help="int, optional. OpenCL platfrom ID")
    parser.add_argument('--cl_device_id', type=int, default=-1,
                        help="int, optional. OpenCL device ID in specified platform")
    parser.add_argument('--float64', action='store_true',
                        help="Calculate with double precision on GPU")
    return parser


class SelfSimVoigt:
    """Self-similarity exploration for transport equation with model 1D core
    See Sec. 3 in A.B. Kukushkin and P.A. Sdvizhenskii, J. Phys. A: Math. Theor. 49 (2016) 255002, doi:10.1088/1751-8113/49/25/255002"""

    def __init__(self, filename='', gamma=0.5, rho=False, rholim=(1.e-30, 1.e30), nx_pp=50, tlim=(30, 1.e8),
                 slim=(0.01, 1.e3), ds=0.02, log_s=True, ns_pp=50, p_powlim=(-16, 2), n_pp=2000, rho_front_old=False,
                 use_gpu=False, cl_platform_id=-1, cl_device_id=-1, float64=False):
        """ filename : string, optional
                Loading object's data from file.
            gamma : float, optional
                Parameter \gamma of the model core (see Eq. 18 in J. Phys. A)
            rho : bool, optional
                If true, rho will be selected as independent variable. Default independent variable is t.
            rholim : tuple, optional
                (Min, Max) values of \rho
            nx_pp : float, optional
                Number of points in numerical grid (log scale) of independent varialbe (t or rho) per power
            tlim : tuple, optional
                (Min, Max) values of t
            slim : tuple, optional
                (Min, Max) values of s
            ds : float, optional
                Step of the s numerical grid
            log_s : bool, optional
                Use logarithmic numerical grid for 's'
            ns_pp : int, optional
                Number of points in 's' numerical grid per power in case of log_s == True.
            p_powlim : tuple, optional
                (Min, Max) values of the power of p. Define the limits of integration by p: (10^Min, 10^Max)
            n_pp : int, optional
                Number of points in p numerical grid (log scale) per power. E.g., if p_powlim = (-8, 2) the total number of points
                in p grid is n_pp * 10
            use_gpu : bool, optional
                Use OpenCL-compatible GPU to accelerate claculations
            cl_platform_id : int, optional
                Index of the OpenCL platform to use. -1 for auto selection
            cl_platform_id : int, optional
                Index of the OpenCL device to use (in the specified platform). -1 for auto selection"""
        self.N_MAX = 134217728  # 1GB limit for any mesh (double)
        self.N_COS = 256
        self.N_EXP = 8192
        self.XLIM_EXP = 12 * np.log(10)  # max value for t * q(p) for error ~ 1.e-7
        self.x = None  # t numerical grid (or list of grids if var == 'rho')
        self.nx = None  # number of points in t grid
        self.s = None  # list of s numerical grids
        self.rho_front = None
        self.q_inner = None  # inner integral array
        self.coeff_small_p = None
        self.pow_small_p = None
        self.es = None  # list of exact solution arrays
        self.auts = None  # list of automodel solution arrays
        self.roots = None  # list of self-similar functions, Q_W(s)
        self.root0 = None  # t(rho)-averaged root (2-column: s, value(s))
        self.self_dir = os.path.dirname(os.path.realpath(__file__))
        success = self.load(filename)
        if not success:
            self.gamma = gamma
            self.rho = rho
            self.nx_pp = nx_pp
            self.tlim = tlim
            self.rholim = rholim
            self.slim = slim
            self.log_s = log_s
            self.ns_pp = ns_pp
            self.ds = ds
            self.p_powlim = p_powlim
            self.n_pp = n_pp
        self.rho_front_old = rho_front_old
        self.use_gpu = use_gpu  # override the value obtained from load()
        self.cl_platform_id = cl_platform_id
        self.cl_device_id = cl_device_id
        self.float64 = float64
        self.xlim = {'t': self.tlim, 'rho': self.rholim}
        self.v = ('rho', 't') if self.rho else ('t', 'rho')
        self.np = int(round(self.n_pp * (self.p_powlim[1] - self.p_powlim[0]))) + 1
        self.p = np.logspace(self.p_powlim[0], self.p_powlim[1], self.np)
        self.dp_pow = float(self.p_powlim[1] - self.p_powlim[0]) / (self.np - 1)
        if self.use_gpu:
            self._cl_init()
        self.omega_log_min = max(100., 10. / self.gamma) if self.gamma else 1.e60
        self.n_omega_linear = 4096 if self.gamma else 8192
        self.omega, self.d_omega_linear, self.d_omega_pow, self.w = self.w_calc()
        self.w_interp = CubicSpline(self.omega, self.w)
        self.w0 = self.w[0]
        self.c_a = 1. / ((self.w[1:] + self.w[:-1]) * (self.omega[1:] - self.omega[:-1])).sum()  # 2x because from -inf to inf
        if self.q_inner is None:
            self.q_inner = self.inner_int()
        self.coeff_small_p, self.pow_small_p = self._small_p()
        self.create_meshes()

    def _cl_init(self):
        """Initializes OpenCL: creates the context, builds the program"""
        if not self.use_gpu:
            return
        from opencl_utils import device_select
        import pyopencl as cl
        device, gflops = device_select(self.cl_platform_id, self.cl_device_id)
        self.cl_context = cl.Context([device])
        self.cl_block_size = 256
        kernel_source_file = os.path.join(self.self_dir, 'SelfSimVoigtKernels.cl')
        compile_options = '-DBlockSize=%d -DN_COS=%d -DN_EXP=%d' % (self.cl_block_size, self.N_COS, self.N_EXP)
        if self.float64:
            compile_options += ' -DFP=double'
        self.cl_prog = cl.Program(self.cl_context, open(kernel_source_file).read()).build(options=compile_options)

    def create_meshes(self):
        """Creates numerical grids"""
        start_create_meshes = timer()
        powlim_up = np.log10(self.xlim[self.v[0]][1])
        powlim_down = np.log10(self.xlim[self.v[0]][0])
        self.nx = int(round((powlim_up - powlim_down) * self.nx_pp)) + 1
        self.x = {}
        self.x[self.v[0]] = np.logspace(powlim_down, powlim_up, self.nx)
        if self.rho:
            s_max = self._slim(self.x['rho'], self.xlim['t'][1])
            s_min = self._slim(self.x['rho'], self.xlim['t'][0])
        else:
            self.rho_front = self._rho_front_old(self.x['t']) if self.rho_front_old else self._rho_front(self.x['t'])
            s_max = self.rho_front / self.xlim['rho'][0]
            s_min = self.rho_front / self.xlim['rho'][1]
        s_max[np.where(s_max > self.slim[1])] = self.slim[1]
        s_min[np.where(s_min < self.slim[0])] = self.slim[0]
        self.s = []
        self.x[self.v[1]] = []
        ind2del = []
        for i in range(self.nx):
            scurr = self._create_mesh_s(s_min[i], s_max[i])
            if not scurr.size:
                ind2del.append(i)
                continue
            self.s.append(scurr)
            if self.rho:
                t_func = np.zeros(scurr.size)
                for j in range(scurr.size):
                    t_func[j] = self.t_calc(self.x['rho'][i] * scurr[j])
                self.x['t'].append(1. / t_func[j] - 1.)
            else:
                self.x['rho'].append(self.rho_front[i] / scurr)
        self.x[self.v[0]] = np.delete(self.x[self.v[0]], ind2del)
        self.nx = self.x[self.v[0]].size
        stop_create_meshes = timer()
        print('create_meshes() took: %g s' % (stop_create_meshes - start_create_meshes))

    def _slim(self, rho, t):
        slim = np.zeros(rho.size)

        def func(x, rhoi):
            return self.t_calc(x * rhoi) * (t + 1.) - 1.

        for i in range(rho.size):
            slim[i] = brentq(func, 0, 1.e16, args=(rho[i],))

        return slim

    def _rho_front_old(self, t):
        rho_fr = np.zeros(t.size)

        def func(x, ti):
            return self.t_calc(x) * (ti + 1.) - 1.

        for it in range(t.size):
            rho_fr[it] = brentq(func, 0, t[it] ** 2, args=(t[it],))

        return rho_fr

    def _rho_front(self, t):
        es0 = self.fexact0_calc()
        if self.use_gpu:
            return self._rho_front_calc_cl(t, es0)
        else:
            return self._rho_front_calc(t, es0)

    def _rho_front_calc(self, t, es0):
        rho_fr = np.zeros(t.size)

        def func(x, ti, es0i):
            return self.automodel_sol_single(x, ti, 1.) / es0i - 1.

        for it in range(t.size):
            rho_fr[it] = brentq(func, 1.e-16, t[it] ** 2, args=(t[it], es0[it]))

        return rho_fr

    def _rho_front_calc_cl(self, t, es0):
        if not self.use_gpu:
            return None
        import pyopencl as cl
        n_groups_max = 16
        # n_per_group_min = self.cl_block_size * 4
        mf = cl.mem_flags
        dtype = np.float64 if self.float64 else np.float32
        byte1 = 8 if self.float64 else 4
        d_omega_host = self.omega[2:] - self.omega[:-2] if self.float64 else (self.omega[2:] - self.omega[:-2]).astype(np.float32)
        d_omega_device = cl.Buffer(self.cl_context, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=d_omega_host)
        w_host = self.w if self.float64 else self.w.astype(np.float32)
        w_device = cl.Buffer(self.cl_context, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=w_host)
        coeff_host = self.w_interp.c.T.flatten() if self.float64 else self.w_interp.c.T.flatten().astype(np.float32)
        cw_device = cl.Buffer(self.cl_context, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=coeff_host)
        res_host = np.zeros(n_groups_max, dtype=dtype)
        res_device = cl.Buffer(self.cl_context, mf.WRITE_ONLY, n_groups_max * byte1)
        queue = cl.CommandQueue(self.cl_context)
        rho_fr = np.zeros(t.size)

        def func(x, ti, es0i):
            return self.automodel_sol_single_cl(x, ti, 1., cl, queue, res_device, w_device, d_omega_device, cw_device,
                                                res_host, n_groups_max) / es0i - 1.

        for it in range(t.size):
            rho_fr[it] = brentq(func, 1.e-16, t[it] ** 2, args=(t[it], es0[it]))
        d_omega_device.release()
        w_device.release()
        cw_device.release()
        res_device.release()

        return rho_fr

    def fexact0_calc(self):
        q_inner_interp = interp1d(np.log10(self.p), np.log10(self.q_inner))
        exp_lim = self.XLIM_EXP / self.x['t']
        res = np.zeros(self.nx)
        for i in range(self.nx):
            ip0 = np.argmax(self.q_inner > exp_lim[i])
            p_lim = self.p[ip0] if ip0 else self.p[-1]
            p1, dp1 = np.linspace(0, p_lim, self.N_EXP, retstep=True)
            ip0 = np.argmax(p1 > self.p[0])
            q = np.zeros(p1.size)
            q[:ip0] = self.coeff_small_p * p1[:ip0]**self.pow_small_p
            q[ip0:] = 10**q_inner_interp(np.log10(p1[ip0:]))
            f = p1 * p1 * (np.exp(- self.x['t'][i] * q) - np.exp(- self.x['t'][i]))
            res[i] = f.sum() * dp1 / (2. * np.pi**2)

        return res

    def t_calc(self, rho):
        y = self.w / self.w0 * rho
        i0 = max(np.argmax(y < self.XLIM_EXP) - 1, 0)
        i1 = np.argmax(y < 0.01)
        if i1:
            d_omega_w = (self.omega[i0 + 1:i1] - self.omega[i0:i1 - 1])
            d_omega_exp = (self.omega[i1] - self.omega[i0]) / self.N_EXP
        if not i1 or d_omega_w[-1] < d_omega_exp:
            f = self.w * np.exp(- y)
            return self.c_a * ((f[1:] + f[:-1]) * (self.omega[1:] - self.omega[:-1])).sum()

        n_omega_fine = int((self.omega[i1] - self.omega[i0]) / min(d_omega_w[0], d_omega_exp)) + 1
        omega_fine, d_omega_fine = np.linspace(self.omega[i0], self.omega[i1], n_omega_fine, retstep=True)
        w_fine = self.w_interp(omega_fine)
        f = w_fine * np.exp(- w_fine / self.w0 * rho)
        f_int = 2. * (f.sum() - 0.5 * f[-1]) * d_omega_fine
        f = self.w[i1:] * np.exp(- y[i1:])
        f_int += ((f[1:] + f[:-1]) * (self.omega[i1 + 1:] - self.omega[i1:-1])).sum()

        return self.c_a * f_int

    def _create_mesh_s(self, smin, smax):
        """Creates numerical grid for s in the range (smin, smax)"""
        if smax < smin:
            return np.array([])
        if self.log_s:
            s_powlim_up = np.log10(smax)
            s_powlim_down = np.log10(smin)
            ns = int(round((s_powlim_up - s_powlim_down) * self.ns_pp)) + 1
            return np.logspace(s_powlim_down, s_powlim_up, ns)

        return np.linspace(smin, smax, int(round((smax - smin) / self.ds)) + 1)

    def load(self, filename):
        """ Loads all parameters and calculated values from a zipfile.
            Return True, if successful.
            filename : str
                Path to a zipfile"""
        if not len(filename):
            return False
        if not zipfile.is_zipfile(filename):
            print("Error: %s is not a zipfile" % filename)
            return False
        with contextlib.closing(zipfile.ZipFile(filename)) as zf:
            namelist = zf.namelist()
            if 'kwargs.csv' in namelist:
                f = zf.open('kwargs.csv')
                for key, val in csv.reader(codecs.iterdecode(f, 'utf-8')):
                    setattr(self, key, eval(val))
            else:
                print("Error: kwargs are not in a zipfile" % filename)
                return False
            if 'q_inner.txt' in namelist:
                f = zf.open('q_inner.txt')
                self.q_inner = np.loadtxt(f)[:, 1]
            if 'exact_sol.txt' in namelist:
                f = zf.open('exact_sol.txt')
                self.es = [np.fromstring(line, sep=' ') for line in f]
            if 'auto_sol.txt' in namelist:
                f = zf.open('auto_sol.txt')
                self.auts = [np.fromstring(line, sep=' ') for line in f]
            if 'roots.txt' in namelist:
                f = zf.open('roots.txt')
                self.roots = [np.fromstring(line, sep=' ') for line in f]
            if 'root0.txt' in namelist:
                f = zf.open('root0.txt')
                self.root0 = np.loadtxt(f)

            return True

    def save(self, dir='', suffix='', extended=False, fmt='%.10e'):
        """ Saves all parameters and calculated values to a zipfile.
            dir : str, optional
                Directory where to save the file.
            suffix : str, optional
                Adds this string to the filename."""
        if len(suffix):
            suffix = '_' + suffix
        if len(dir) and not os.path.isdir(dir):
            os.makedirs(dir)
        filename = os.path.join(dir, 'selfsim_%s_%g-%g_gamma_%g%s.zip' % (self.v[0], self.xlim[self.v[0]][0],
                                                                          self.xlim[self.v[0]][1], self.gamma, suffix))
        with contextlib.closing(zipfile.ZipFile(filename, 'w', compression=zipfile.ZIP_DEFLATED)) as zf:
            kwargs = self._params2dict()
            out = StringIO()
            csvw = csv.writer(out)
            for key in kwargs.keys():
                csvw.writerow([key, kwargs[key]])
            zf.writestr('kwargs.csv', out.getvalue())
            if self.q_inner is not None:
                out = outIO()
                np.savetxt(out, np.array([self.p, self.q_inner]).T, fmt=fmt)
                zf.writestr('q_inner.txt', out.getvalue())
            if self.es is not None:
                self._save_list(zf, self.es, 'exact_sol', fmt=fmt)
            if self.auts is not None:
                self._save_list(zf, self.auts, 'auto_sol', fmt=fmt)
            if self.roots is not None:
                self._save_list(zf, self.roots, 'roots', fmt=fmt)
            if self.root0 is not None:
                out = outIO()
                np.savetxt(out, self.root0, fmt=fmt)
                zf.writestr('root0.txt', out.getvalue())
            if not extended:
                return
            if self.s is not None:
                self._save_list(zf, self.s, 's', fmt=fmt)
            if self.x is not None:
                out = outIO()
                np.savetxt(out, self.x[self.v[0]], fmt=fmt)
                zf.writestr('%s.txt' % self.v[0], out.getvalue())
                self._save_list(zf, self.x[self.v[1]], self.v[1], fmt=fmt)

    def _save_list(self, zf, x, name, fmt='%.10e'):
        """Saves the list of arrays 'x' in the zipfile object 'zf' under the name 'name'.txt"""
        out = outIO()
        for el in x:
            np.savetxt(out, el.reshape(1, el.shape[0]), fmt=fmt)
        zf.writestr('%s.txt' % name, out.getvalue())

    def _params2dict(self):
        """converts arguments into dict"""
        kwargs = {}
        kwargs['gamma'] = self.gamma
        kwargs['rho'] = self.rho
        kwargs['rholim'] = self.rholim
        kwargs['nx_pp'] = self.nx_pp
        kwargs['tlim'] = self.tlim
        kwargs['slim'] = self.slim
        kwargs['ds'] = self.ds
        kwargs['log_s'] = self.log_s
        kwargs['ns_pp'] = self.ns_pp
        kwargs['p_powlim'] = self.p_powlim
        kwargs['n_pp'] = self.n_pp
        kwargs['use_gpu'] = self.use_gpu
        kwargs['cl_platform_id'] = self.cl_platform_id
        kwargs['cl_device_id'] = self.cl_device_id
        kwargs['float64'] = self.float64

        return kwargs

    def _w_int(self, omega):
        nx_exp = 2048
        nx_denom = 1024
        xmax = 8. / (self.gamma * np.sqrt(2.))
        dx_exp = 2 * xmax / (nx_exp + 1)
        dx_denom = 10. / (nx_denom + 1)
        x, dx = np.linspace(-xmax, xmax, nx_exp, retstep=True)
        if dx_exp < dx_denom:
            res = dx * (np.exp(-(self.gamma * x)**2)[None, :] / (1 + (omega[:, None] - x[None, :])**2)).sum(1)
            return res
        res = np.zeros(omega.size)
        xpow_max = np.log10(omega[-1] + xmax)
        x0 = np.hstack((np.linspace(0, 10., nx_denom, endpoint=False), np.logspace(1., xpow_max, nx_denom * (xpow_max - 1.))))
        x0 = np.hstack((-x0[:0:-1], x0))
        for i in range(omega.size):
            x1 = omega[i] + x0
            x1 = x1[(x1 >= -xmax) * (x1 <= xmax)]
            if x1.size:
                dx1 = x1[1:] - x1[:-1]
                ind, = np.where(dx1 < dx_exp)
            else:
                ind = np.zeros(0)
            if ind.size:
                dx_norm = dx1[ind[0]:ind[-1] + 1]
                x1_norm = x1[ind[0]:ind[-1] + 2]
                f = np.exp(-(self.gamma * x1_norm)**2) / (1 + (omega[i] - x1_norm)**2)
                res[i] += 0.5 * ((f[1:] + f[:-1]) * dx_norm).sum()
                if ind[0]:
                    nx_fine = int((x1[ind[0]] + xmax) / dx_exp) + 1
                    x1_fine, dx_fine = np.linspace(-xmax, x1[ind[0]], nx_fine, retstep=True)
                    f = np.exp(-(self.gamma * x1_fine)**2) / (1 + (omega[i] - x1_fine)**2)
                    res[i] += (f.sum() - 0.5 * f[-1]) * dx_fine
                if ind[-1] < x1.size - 2:
                    nx_fine = int((xmax - x1[ind[-1] + 1]) / dx_exp) + 1
                    x1_fine, dx_fine = np.linspace(x1[ind[-1] + 1], xmax, nx_fine, retstep=True)
                    f = np.exp(-(self.gamma * x1_fine)**2) / (1 + (omega[i] - x1_fine)**2)
                    res[i] += (f.sum() - 0.5 * f[0]) * dx_fine
            else:
                res[i] = (np.exp(-(self.gamma * x)**2) / (1 + (omega[i] - x)**2)).sum() * dx

        return res

    def w_calc(self):
        if self.gamma == 0:
            omega, d_omega_linear = np.linspace(0, 16 / np.sqrt(2.), self.n_omega_linear, endpoint=False, retstep=True)
            w = np.exp(- omega * omega)
            d_omega_log = 0
            nom_log = 0
        else:
            w0 = np.pi * erfc(self.gamma) * np.exp(self.gamma**2) if self.gamma < 20. else self._w_int(np.zeros(1))[0]
            omega_max = max(1.e8, 1.e4 * np.sqrt(np.sqrt(np.pi) / (w0 * self.gamma * self.p[0])))
            omega_max_pow = np.log10(omega_max)
            omega_log_min_pow = np.log10(self.omega_log_min)
            nom_log = int(round(1024 * (omega_max_pow - omega_log_min_pow))) + 1
            omega_linear, d_omega_linear = np.linspace(0, self.omega_log_min, self.n_omega_linear, endpoint=False, retstep=True)
            omega_log = np.logspace(omega_log_min_pow, omega_max_pow, nom_log)
            d_omega_log = (omega_max_pow - omega_log_min_pow) / (nom_log - 1)
            omega = np.hstack((omega_linear, omega_log))
            w = self._w_int(omega)

        return (omega, d_omega_linear, d_omega_log, w)

    def inner_int(self):
        start_inner_int = timer()
        if self.use_gpu:
            q_inner = self._integrate_inner_int_cl()
        else:
            q_inner = self._integrate_inner_int()
        stop_inner_int = timer()
        print('inner_int() took: %g s' % (stop_inner_int - start_inner_int))

        return q_inner

    def _integrate_inner_int(self):
        ip0 = np.argmax(self.p > 1.e-4)
        q_inner = np.zeros(self.np)
        for ip in range(ip0):
            indx, = np.where(self.p[ip] * self.w0 / self.w > 1.e-2)
            iw0 = indx[0] if indx.size else self.w.size - 1
            if iw0 > 0:
                f = 1. / self.w[:iw0 + 1]
                q_inner[ip] += (self.c_a * (self.w0 * self.p[ip])**2 / 3.) *\
                    ((f[1:] + f[:-1]) * (self.omega[1:iw0 + 1] - self.omega[:iw0])).sum()
                f = 1. / self.w[:iw0 + 1]**3
                q_inner[ip] -= (self.c_a * (self.w0 * self.p[ip])**4 / 5.) *\
                    ((f[1:] + f[:-1]) * (self.omega[1:iw0 + 1] - self.omega[:iw0])).sum()
            if iw0 < self.w.size - 1:
                f = self.w[iw0:] ** 2 * np.arctan(self.p[ip] * self.w0 / self.w[iw0:])
                q_inner[ip] -= self.c_a / (self.p[ip] * self.w0) * ((f[1:] + f[:-1]) * (self.omega[iw0 + 1:] - self.omega[iw0:-1])).sum()
                q_inner[ip] += self.c_a * ((self.w[iw0 + 1:] + self.w[iw0:-1]) * (self.omega[iw0 + 1:] - self.omega[iw0:-1])).sum()
        for ip in range(ip0, self.np):
            f = self.w ** 2 * np.arctan(self.p[ip] * self.w0 / self.w)
            q_inner[ip] = 1. - self.c_a / (self.p[ip] * self.w0) * ((f[1:] + f[:-1]) * (self.omega[1:] - self.omega[:-1])).sum()

        return q_inner

    def _integrate_inner_int_cl(self):
        if not self.use_gpu:
            return None
        import pyopencl as cl
        ip0 = np.argmax(self.p > 1.e-4)
        mf = cl.mem_flags
        dtype = np.float64 if self.float64 else np.float32
        q_host = np.zeros(self.np, dtype=dtype)
        q_device = cl.Buffer(self.cl_context, mf.READ_WRITE | mf.COPY_HOST_PTR, hostbuf=q_host)
        w_host = self.w if self.float64 else self.w.astype(np.float32)
        w_device = cl.Buffer(self.cl_context, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=w_host)
        omega_host = self.omega if self.float64 else self.omega.astype(np.float32)
        omega_device = cl.Buffer(self.cl_context, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=omega_host)
        ip0 = np.argmax(self.p > 1.e-4)
        local_work_size = (self.cl_block_size, 1)
        n_work_groups = ip0 // self.cl_block_size + bool(ip0 % self.cl_block_size)
        global_work_size_small_p = (n_work_groups * self.cl_block_size, 1)
        n_work_groups = (self.np - ip0) // self.cl_block_size + bool((self.np - ip0) % self.cl_block_size)
        global_work_size_large_p = (n_work_groups * self.cl_block_size, 1)
        queue = cl.CommandQueue(self.cl_context)
        self.cl_prog.inner_int_small_p(queue, global_work_size_small_p, local_work_size,
                                       q_device, w_device, omega_device, dtype(self.p_powlim[0]),
                                       dtype(self.dp_pow), dtype(self.w0), dtype(self.c_a),
                                       np.uint32(ip0), np.uint32(0), np.uint32(self.w.size))
        self.cl_prog.inner_int_large_p(queue, global_work_size_large_p, local_work_size,
                                       q_device, w_device, omega_device, dtype(self.p_powlim[0]),
                                       dtype(self.dp_pow), dtype(self.w0), dtype(self.c_a),
                                       np.uint32(ip0), np.uint32(self.np), np.uint32(0), np.uint32(self.w.size))
        cl.enqueue_copy(queue, q_host, q_device)
        q_device.release()
        w_device.release()
        omega_device.release()
        q_host[ip0:] = 1. - q_host[ip0:]

        return q_host.astype(np.float64)

    def _small_p(self):
        plog = np.log(self.p)
        qlog = np.log(self.q_inner)
        pow_small_p = (qlog[self.n_pp // 2 - 1] - qlog[0]) / (plog[self.n_pp // 2 - 1] - plog[0])
        coeff_small_p = np.exp(qlog[0] - pow_small_p * plog[0])

        return (coeff_small_p, pow_small_p)

    def exact_sol(self):
        """ Computes the exact solutions. Stores the results as a list of arrays in self.es.
            Returns self.es"""
        start_exact_sol = timer()
        if self.use_gpu:
            self.es = self._integrate_exact_sol_cl()
        else:
            self.es = self._integrate_exact_sol()
        stop_exact_sol = timer()
        print('exact_sol() took: %g s' % (stop_exact_sol - start_exact_sol))

        return self.es

    def _integrate_exact_sol(self):
        q_inner_interp = interp1d(np.log10(self.p), np.log10(self.q_inner))
        if self.rho:
            es = self._integrate_exact_sol_rho(q_inner_interp)
        else:
            es = self._integrate_exact_sol_t(q_inner_interp)

        return es

    def _integrate_exact_sol_rho(self, q_inner_interp):
        es = []
        dp_cos = 0.5 * np.pi / (self.x['rho'] * self.N_COS)
        for i in range(self.nx):
            exp_lim = self.XLIM_EXP / self.x['t'][i]
            res = np.zeros(self.x['t'][i].size)
            for j in range(self.x['t'][i].size):
                ip0 = np.argmax(self.q_inner > exp_lim[j])
                p_lim = self.p[ip0] if ip0 else self.p[-1]
                dp_exp = p_lim / self.N_EXP
                dp = min(dp_exp, dp_cos[i])
                res[j] = self._integrate_exact_sol_single_t_rho(self.x['rho'][i], self.x['t'][i][j], q_inner_interp, p_lim, dp)
            es.append(res)

        return es

    def _integrate_exact_sol_t(self, q_inner_interp):
        es = []
        exp_lim = self.XLIM_EXP / self.x['t']
        for i in range(self.nx):
            # print(self.x['t'][i])
            ip0 = np.argmax(self.q_inner > exp_lim[i])
            p_lim = self.p[ip0] if ip0 else self.p[-1]
            dp_exp = p_lim / self.N_EXP
            dp_cos = 0.5 * np.pi / (self.x['rho'][i] * self.N_COS)
            res = np.zeros(self.x['rho'][i].size)
            for j in range(self.x['rho'][i].size):
                dp = min(dp_exp, dp_cos[j])
                res[j] = self._integrate_exact_sol_single_t_rho(self.x['rho'][i][j], self.x['t'][i], q_inner_interp, p_lim, dp)
                # print(self.x['t'][i], self.x['rho'][i][j], res[j])
            es.append(res)

        return es

    def _integrate_exact_sol_single_t_rho(self, rho, t, q_inner_interp, p_lim, dp):
        """ Computes the p-integral in the exact solution for given t and rho"""
        np_total = int(p_lim / dp) + 1
        niter = 1 + np_total // self.N_MAX
        res = 0
        for it in range(niter):
            p_min = it * self.N_MAX * dp
            p_max = min(p_lim, (it + 1) * self.N_MAX * dp)
            np_mesh = int((p_max - p_min) / dp) + 1
            p1, dp1 = np.linspace(p_min, p_max, np_mesh, retstep=True)
            if self.p[0] > p_max:
                q = self.coeff_small_p * p1**self.pow_small_p
            elif self.p[0] < p_min:
                q = 10**q_inner_interp(np.log10(p1))
            else:
                ip0 = np.argmax(p1 > self.p[0])
                q = np.zeros(p1.size)
                q[:ip0] = self.coeff_small_p * p1[:ip0]**self.pow_small_p
                q[ip0:] = 10**q_inner_interp(np.log10(p1[ip0:]))
            f = p1 * np.sin(rho * p1) * (np.exp(- t * q) - np.exp(- t))
            res1 = (f[1:-1].sum() + 0.5 * (f[0] + f[-1])) * dp1
            res += res1

        return res / (2. * np.pi**2 * rho)

    def _integrate_exact_sol_cl(self):
        """ Computes the p-integral in the exact solution on GPU"""
        if not self.use_gpu:
            return None
        import pyopencl as cl
        n_groups_max = 128
        es = []
        if self.rho:
            dp_cos = 0.5 * np.pi / (self.x['rho'] * self.N_COS)
        else:
            exp_lim = self.XLIM_EXP / self.x['t']
        mf = cl.mem_flags
        dtype = np.float64 if self.float64 else np.float32
        byte1 = 8 if self.float64 else 4
        q_host = self.q_inner if self.float64 else self.q_inner.astype(np.float32)
        q_device = cl.Buffer(self.cl_context, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=q_host)
        nbuff = 0
        for i in range(self.nx):
            nbuff = max(self.s[i].size, nbuff)
        nbuff *= n_groups_max
        queue = cl.CommandQueue(self.cl_context)
        res_device = cl.Buffer(self.cl_context, mf.WRITE_ONLY, nbuff * byte1)
        res_host = np.zeros(nbuff, dtype=dtype)
        n_groups_zero = nbuff // self.cl_block_size + bool(nbuff % self.cl_block_size)
        local_work_size = (self.cl_block_size, 1)
        for i in range(self.nx):
            self.cl_prog.zero_buffer(queue, (n_groups_zero * self.cl_block_size, 1), local_work_size, res_device, np.uint32(nbuff))
            if self.rho:
                exp_lim = self.XLIM_EXP / self.x['t'][i]
                for j in range(self.s[i].size):
                    ip0 = np.argmax(self.q_inner > exp_lim[j])
                    plim = self.p[ip0] if ip0 else self.p[-1]
                    dp = min(plim / self.N_EXP, dp_cos[i])
                    np_total = int(plim / dp) + 1
                    np_per_block_size = np_total // self.cl_block_size + bool(np_total % self.cl_block_size)
                    n_groups = min(np_per_block_size, n_groups_max)
                    np_per_group = self.cl_block_size * (np_per_block_size // n_groups + bool(np_per_block_size % n_groups))
                    global_work_size = (n_groups * self.cl_block_size, 1)
                    # np_per_group = np_total // n_groups + bool(np_total % n_groups)
                    self.cl_prog.exact_sol_t_rho_int(queue, global_work_size, local_work_size,
                                                     res_device, q_device, np.uint32(j * n_groups_max), dtype(self.x['rho'][i][j]),
                                                     dtype(self.x['t'][i]), np.uint64(np_per_group), np.uint64(np_total),
                                                     dtype(dp), dtype(self.p_powlim[0]),
                                                     dtype(self.dp_pow), dtype(self.coeff_small_p), dtype(self.pow_small_p))
            else:
                ip0 = np.argmax(self.q_inner > exp_lim[i])
                plim = self.p[ip0] if ip0 else self.p[-1]
                dp_exp = plim / self.N_EXP
                for j in range(self.s[i].size):
                    dp = min(dp_exp, 0.5 * np.pi / (self.x['rho'][i][j] * self.N_COS))
                    np_total = int(plim / dp) + 1
                    np_per_block_size = np_total // self.cl_block_size + bool(np_total % self.cl_block_size)
                    n_groups = min(np_per_block_size, n_groups_max)
                    np_per_group = self.cl_block_size * (np_per_block_size // n_groups + bool(np_per_block_size % n_groups))
                    global_work_size = (n_groups * self.cl_block_size, 1)
                    # np_per_group = np_total // n_groups + bool(np_total % n_groups)
                    self.cl_prog.exact_sol_t_rho_int(queue, global_work_size, local_work_size,
                                                     res_device, q_device, np.uint32(j * n_groups_max), dtype(self.x['rho'][i][j]),
                                                     dtype(self.x['t'][i]), np.uint64(np_per_group), np.uint64(np_total),
                                                     dtype(dp), dtype(self.p_powlim[0]),
                                                     dtype(self.dp_pow), dtype(self.coeff_small_p), dtype(self.pow_small_p))
            nsize = self.s[i].size * n_groups_max
            cl.enqueue_copy(queue, res_host[:nsize], res_device)
            # print(i)
            es.append(res_host[:nsize].reshape((self.s[i].size, n_groups_max)).sum(1).astype(np.float64) / (2. * np.pi**2))
        res_device.release()
        q_device.release()

        return es

    def automodel_sol(self):
        """ Computes automodel solutions. Stores the results as a list of arrays in self.auts.
            Returns self.auts"""
        if self.roots is None:
            self.root()
        start_auto_sol = timer()
        root0i = interp1d(self.root0[:, 0], self.root0[:, 1], bounds_error=False, fill_value=(self.root0[0, 1], self.root0[-1, 1]))
        if self.use_gpu:
            self.auts = self._calc_automodel_sol_cl(root0i)
        else:
            self.auts = self._calc_automodel_sol(root0i)
        stop_auto_sol = timer()
        print('automodel_sol() took: %g s' % (stop_auto_sol - start_auto_sol))

        return self.auts

    def _calc_automodel_sol(self, root0i):
        auts = []
        for i in range(len(self.es)):
            auts1 = np.zeros(self.es[i].size)
            root0s = root0i(self.s[i])
            for j in range(self.es[i].size):
                if self.rho:
                    auts1[j] = self.automodel_sol_single(root0s[j], self.x['t'][i][j], self.x['rho'][i])
                else:
                    auts1[j] = self.automodel_sol_single(root0s[j], self.x['t'][i], self.x['rho'][i][j])
            auts.append(auts1)

        return auts

    def _calc_automodel_sol_cl(self, root0i):
        if not self.use_gpu:
            return None
        import pyopencl as cl
        n_groups_max = 32
        # n_per_group_min = self.cl_block_size * 4
        mf = cl.mem_flags
        dtype = np.float64 if self.float64 else np.float32
        byte1 = 8 if self.float64 else 4
        d_omega_host = self.omega[2:] - self.omega[:-2] if self.float64 else (self.omega[2:] - self.omega[:-2]).astype(np.float32)
        d_omega_device = cl.Buffer(self.cl_context, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=d_omega_host)
        w_host = self.w if self.float64 else self.w.astype(np.float32)
        w_device = cl.Buffer(self.cl_context, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=w_host)
        coeff_host = self.w_interp.c.T.flatten() if self.float64 else self.w_interp.c.T.flatten().astype(np.float32)
        cw_device = cl.Buffer(self.cl_context, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=coeff_host)
        res_host = np.zeros(n_groups_max, dtype=dtype)
        res_device = cl.Buffer(self.cl_context, mf.WRITE_ONLY, n_groups_max * byte1)
        queue = cl.CommandQueue(self.cl_context)
        auts = []
        for i in range(len(self.es)):
            auts1 = np.zeros(self.es[i].size)
            root0s = root0i(self.s[i])
            for j in range(self.es[i].size):
                if self.rho:
                    auts1[j] = self.automodel_sol_single_cl(root0s[j], self.x['t'][i][j], self.x['rho'][i], cl, queue,
                                                            res_device, w_device, d_omega_device, cw_device, res_host, n_groups_max)
                else:
                    auts1[j] = self.automodel_sol_single_cl(root0s[j], self.x['t'][i], self.x['rho'][i][j], cl, queue,
                                                            res_device, w_device, d_omega_device, cw_device, res_host, n_groups_max)
            auts.append(auts1)
        d_omega_device.release()
        w_device.release()
        cw_device.release()
        res_device.release()

        return auts

    def automodel_sol_single(self, root, t, rho):
        y = self.w / self.w0 * root * rho
        i0 = max(np.argmax(y < self.XLIM_EXP) - 1, 0)
        i1 = np.argmax(y < 0.01)
        i2 = (i1 + self.omega.size) // 2
        if i1:
            d_omega_w = (self.omega[i0 + 1:i1] - self.omega[i0:i1 - 1])
            d_omega_exp = (self.omega[i1] - self.omega[i0]) / self.N_EXP
        if not i1 or d_omega_w[-1] < d_omega_exp:
            f = self.w * self.w * np.exp(- y)
            f_int = ((f[1:] + f[:-1]) * (self.omega[1:] - self.omega[:-1])).sum()

            return self.c_a * t / (4. * np.pi * rho**2 * root**2 * self.w0) * f_int

        n_omega_fine = int((self.omega[i1] - self.omega[i0]) / min(d_omega_w[0], d_omega_exp)) + 1
        omega_fine, d_omega_fine = np.linspace(self.omega[i0], self.omega[i1], n_omega_fine, retstep=True)
        w_fine = self.w_interp(omega_fine)
        f = w_fine * w_fine * np.exp(- w_fine / self.w0 * root * rho)
        f_int = (2. * f.sum() - f[-1] - f[0]) * d_omega_fine
        f = self.w[i1:i2] * self.w[i1:i2] * np.exp(- y[i1:i2])
        f_int += ((f[1:] + f[:-1]) * (self.omega[i1 + 1:i2] - self.omega[i1:i2 - 1])).sum()

        return self.c_a * t / (4. * np.pi * rho**2 * root**2 * self.w0) * f_int

    def automodel_sol_single_cl(self, root, t, rho, cl, queue, res_device, w_device, d_omega_device, cw_device, res_host, n_groups_max):
        dtype = np.float64 if self.float64 else np.float32
        y = self.w / self.w0
        local_work_size = (self.cl_block_size, 1)
        rho1 = root * rho
        i0 = max(np.argmax(y * rho1 < self.XLIM_EXP) - 1, 0)
        i1 = np.argmax(y * rho1 < 0.01)
        i2 = (i1 + self.omega.size) // 2
        if i1:
            d_om_w0 = (self.omega[i0 + 1] - self.omega[i0])
            d_om_w1 = (self.omega[i1 - 1] - self.omega[i1 - 2])
            d_om_exp = (self.omega[i1] - self.omega[i0]) / self.N_EXP
            # print(d_om_w1, d_om_exp)
        if not i1 or d_om_w1 < d_om_exp:
            n_per_block_size = (self.w.size - 2) // self.cl_block_size + bool((self.w.size - 2) % self.cl_block_size)
            n_groups = min(n_per_block_size, n_groups_max)
            n_per_group = self.cl_block_size * (n_per_block_size // n_groups + bool(n_per_block_size % n_groups))
            global_work_size = (n_groups * self.cl_block_size, 1)
            self.cl_prog.automodel_calc(queue, global_work_size, local_work_size, res_device, w_device, d_omega_device,
                                        dtype(rho1), dtype(t), dtype(self.w0), dtype(self.c_a),
                                        np.uint32(n_per_group), np.uint32(1), np.uint32(self.w.size - 1))
            f0 = self.c_a * t / (4. * np.pi * rho1**2 * self.w0) * self.w0 * self.w0 * np.exp(- rho1)
            cl.enqueue_copy(queue, res_host, res_device)

            return (res_host[:n_groups].sum() + f0 * (self.omega[1] - self.omega[0]))
        else:
            n_per_block_size = (i2 - i1 - 2) // self.cl_block_size + bool((i2 - i1 - 2) % self.cl_block_size)
            n_groups = min(n_per_block_size, n_groups_max)
            n_per_group = self.cl_block_size * (n_per_block_size // n_groups + bool(n_per_block_size % n_groups))
            global_work_size = (n_groups * self.cl_block_size, 1)
            self.cl_prog.automodel_calc(queue, global_work_size, local_work_size, res_device, w_device, d_omega_device,
                                        dtype(rho1), dtype(t), dtype(self.w0), dtype(self.c_a),
                                        np.uint32(n_per_group), np.uint32(i1 + 1), np.uint32(i2 - 1))
            f_i1 = self.c_a * t / (4. * np.pi * rho1**2 * self.w0) * self.w[i1] * self.w[i1] * np.exp(- self.w[i1] * rho1 / self.w0)
            f_i0 = self.c_a * t / (4. * np.pi * rho1**2 * self.w0) * self.w[i0] * self.w[i0] * np.exp(- self.w[i0] * rho1 / self.w0)
            n_omega_fine = int((self.omega[i1] - self.omega[i0]) / min(d_om_w0, d_om_exp)) + 1
            d_om_fine = (self.omega[i1] - self.omega[i0]) / (n_omega_fine - 1)
            cl.enqueue_copy(queue, res_host, res_device)
            res = res_host[:n_groups].sum()
            n_per_block_size = n_omega_fine // self.cl_block_size + bool(n_omega_fine % self.cl_block_size)
            n_groups = min(n_per_block_size, n_groups_max)
            n_per_group = self.cl_block_size * (n_per_block_size // n_groups + bool(n_per_block_size % n_groups))
            global_work_size = (n_groups * self.cl_block_size, 1)
            self.cl_prog.automodel_calc_fine(queue, global_work_size, local_work_size, res_device,
                                             cw_device, dtype(rho1), dtype(t), dtype(self.w0), dtype(self.c_a),
                                             np.uint32(n_per_group), np.uint32(n_omega_fine),
                                             dtype(d_om_fine), dtype(self.omega[i0]), dtype(self.omega_log_min), np.uint32(self.n_omega_linear),
                                             dtype(self.d_omega_linear), dtype(self.d_omega_pow))
            cl.enqueue_copy(queue, res_host, res_device)
            res += res_host[:n_groups].sum()

            return (res + f_i1 * (self.omega[i1 + 1] - self.omega[i1] - d_om_fine) - f_i0 * d_om_fine)

    def root(self):
        """ Solves the equation to find the self-similar functions. Stores the results as a list of arrays in self.roots.
            Returns self.roots
            limits : tuple, optional
                (Min, Max) values of the possible roots of the equation (and of the self-similar function)"""
        if self.es is None:
            self.exact_sol()
        start_root = timer()
        if self.use_gpu:
            self.roots = self._calc_roots_cl()
        else:
            self.roots = self._calc_roots()
        self.root0 = self._calc_root0()
        stop_root = timer()
        print('Root() took: %g s' % (stop_root - start_root))

        return self.roots

    def _calc_roots(self):
        roots = []

        def func(x, t, rho, es):

            return self.automodel_sol_single(x, t, rho) / es - 1.

        for i in range(len(self.es)):
            # print(i)
            x = np.zeros(self.es[i].size)
            for j in range(self.es[i].size):
                if self.rho:
                    x[j] = brentq(func, 0.5, max(2, 10 * self.s[i][j]), args=(self.x['t'][i][j], self.x['rho'][i], abs(self.es[i][j])))
                else:
                    x[j] = brentq(func, 0.5, max(2, 10 * self.s[i][j]), args=(self.x['t'][i], self.x['rho'][i][j], abs(self.es[i][j])))
            roots.append(x)

        return roots

    def _calc_roots_cl(self):
        if not self.use_gpu:
            return None
        import pyopencl as cl
        n_groups_max = 32
        # n_per_group_min = self.cl_block_size * 4
        roots = []
        mf = cl.mem_flags
        dtype = np.float64 if self.float64 else np.float32
        byte1 = 8 if self.float64 else 4
        d_omega_host = self.omega[2:] - self.omega[:-2] if self.float64 else (self.omega[2:] - self.omega[:-2]).astype(np.float32)
        d_omega_device = cl.Buffer(self.cl_context, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=d_omega_host)
        w_host = self.w if self.float64 else self.w.astype(np.float32)
        w_device = cl.Buffer(self.cl_context, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=w_host)
        coeff_host = self.w_interp.c.T.flatten() if self.float64 else self.w_interp.c.T.flatten().astype(np.float32)
        cw_device = cl.Buffer(self.cl_context, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=coeff_host)
        res_host = np.zeros(n_groups_max, dtype=dtype)
        res_device = cl.Buffer(self.cl_context, mf.WRITE_ONLY, n_groups_max * byte1)
        queue = cl.CommandQueue(self.cl_context)

        def func(x, t, rho, es):

            return self.automodel_sol_single_cl(x, t, rho, cl, queue, res_device, w_device, d_omega_device, cw_device,
                                                res_host, n_groups_max) / es - 1.

        for i in range(len(self.es)):
            # print(i)
            x1 = np.zeros(self.es[i].size)
            for j in range(self.es[i].size):
                if self.rho:
                    x1[j] = brentq(func, 0.5, max(2, 10 * self.s[i][j]), args=(self.x['t'][i][j], self.x['rho'][i], abs(self.es[i][j])))
                else:
                    x1[j] = brentq(func, 0.5, max(2, 10 * self.s[i][j]), args=(self.x['t'][i], self.x['rho'][i][j], abs(self.es[i][j])))
            roots.append(x1)

        d_omega_device.release()
        w_device.release()
        cw_device.release()
        res_device.release()

        return roots

    def _calc_root0(self):
        """Computes g(s) for automodel solution averaged over independent variable"""
        x = self.x[self.v[0]]
        smin = 1e9
        smax = 0
        for i in range(x.size):
            smin = min(smin, self.s[i][0])
            smax = max(smax, self.s[i][-1])
        s0 = self._create_mesh_s(smin + 1.e-10, smax - 1.e-10)
        root0 = np.zeros((s0.size, 2))
        root0[:, 0] = s0
        weights = np.zeros(s0.size)
        weight_x = np.zeros(x.size)
        weight_x[1:-1] = 0.5 * (x[2:] - x[:-2])
        weight_x[0] = 0.5 * (x[1] - x[0])
        weight_x[-1] = 0.5 * (x[-1] - x[-2])
        for i in range(x.size):
            if self.s[i].size > 1:
                rooti = interp1d(self.s[i], self.roots[i], bounds_error=False, fill_value=0)(s0)
                root0[:, 1] += weight_x[i] * rooti
                weights += weight_x[i] * (rooti > 0)
        root0[:, 1] /= weights

        return root0

    def plot_inner_int(self, interactive=True, save=False, dir='', suffix='', fmt='png', dpi=300):
        """ Plots the inner integral in Eq. (18) in J. Phys. A as a function of p.
            Returns matplotlib figure
            interactive : bool, optional
                If true, creates interactive figure. Set to False for servers without X.
            save : bool, optional
                Save the image or not.
            dir : str, optional
                Directory where to save the image.
            suffix : str, optional
                Adds this string to image filename.
            format : str, optional
                Image format: png, jpg, eps, ...
            dpi : int, optional
                Dots per inch for saved image."""
        if len(suffix):
            suffix = '_' + suffix
        if not interactive:
            import matplotlib as mpl
            mpl.use('Agg')
            save = True
        from matplotlib import pyplot as plt
        if self.q_inner is None:
            self.inner_int()
        fig = plt.figure(figsize=(5., 4.), facecolor='w')
        ax = fig.add_axes([0.14, 0.13, 0.84, 0.85])
        ax.plot(self.p, self.q_inner, zorder=2)
        ax.plot([self.p[0], self.p[-1]], [1., 1.], color='0.6', ls='--', zorder=1)
        ax.text(0.95, 0.05, r'$\gamma$ = %g' % self.gamma, ha='right', transform=ax.transAxes)
        # ax.set_xlim(1.e-3, 1000)
        ax.set_xscale('log')
        ax.set_yscale('log')
        ax.set_xlabel('p')
        ax.set_ylabel('Q$_{inner}$')
        if save:
            if len(dir) and not os.path.isdir(dir):
                os.makedirs(dir)
            fig.savefig(os.path.join('%s' % dir, 'inner_int_gamma_%g%s.%s' % (self.gamma, suffix, fmt)), dpi=dpi)

        return fig

    def plot_es(self, value_list, interactive=True, save=False, dir='', suffix='', fmt='png', dpi=300):
        """ Plots the exact solutions for the given values of \rho (if var == 'rho') or t (if var == 't') as functions of s.
            Returns matplotlib figure
            value_list : iterable
                List of \rho (or t) values within the rholim (or tlim) interval for which the exact solutions will be plotted
            interactive : bool, optional
                If true, creates interactive figure. Set to False for servers without X.
            save : bool, optional
                Save the image or not.
            dir : str, optional
                Directory where to save the image.
            suffix : str, optional
                Adds this string to image filename.
            format : str, optional
                Image format: png, jpg, eps, ...
            dpi : int, optional
                Dots per inch for saved image."""
        if len(suffix):
            suffix = '_' + suffix
        if not interactive:
            import matplotlib as mpl
            mpl.use('Agg')
            save = True
        from matplotlib import pyplot as plt
        x = self.x[self.v[0]]
        if self.es is None:
            self.exact_sol()
        fig = plt.figure(figsize=(5., 4.), facecolor='w')
        ax = fig.add_axes([0.14, 0.13, 0.84, 0.8])
        iclist = []
        pow_max = -250
        for val in value_list:
            if not x[0] <= val <= x[-1]:
                print('Plotting warning: value %g is out of the range (%g, %g)' % (val, x[0], x[-1]))
                continue
            ic = np.argmin(np.abs(val - x))  # index of the closest value
            if ic in iclist:
                continue
            iclist.append(ic)
            pow_max = max(int(np.log10(self.es[ic].max())) - 1, pow_max)
        for ic in iclist:
            ax.plot(self.s[ic], self.es[ic] * 10.**(-pow_max), label='%g' % x[ic])
        ax.set_title(r'$\gamma$ = %g' % self.gamma)
        ax.legend(loc=0, frameon=False, ncol=len(value_list) // 4 + 1)
        if self.log_s:
            ax.set_xscale('log')
        ax.set_xlabel('s')
        ax.set_ylabel('f$_{exact}$, 10$^{%d}$' % pow_max)
        if save:
            if len(dir) and not os.path.isdir(dir):
                os.makedirs(dir)
            fig.savefig(os.path.join('%s' % dir, 'exact_sol_%s_gamma_%g_%s%s.%s' % (self.v[0], self.gamma,
                                                                                    '_'.join(['%g' % val for val in value_list]),
                                                                                    suffix, fmt)), dpi=dpi)

        return fig

    def plot_roots_3d(self, step=1, xlim=(None, None), slim=(None, None), logscale=True, interactive=True, save=False,
                      dir='', suffix='', fmt='png', dpi=300):
        """ Plots the calculated self similar functions (roots) in the 3D space.
            Returns matplotlib figure
            step : int, optional
                If specified, only each i-th == step self similar function will be plotted
            xlim : tuple of 2 floats, optional
                Plot roots only in the interval xlim[0] < rho (or t) < xlim[1]
            slim : tuple of 2 floats, optional
                Plot roots only in the interval slim[0] < s < slim[1]
            interactive : bool, optional
                If true, creates interactive figure. Set to False for servers without X.
            save : bool, optional
                Save the image or not.
            dir : str, optional
                Directory where to save the image.
            suffix : str, optional
                Adds this string to image filename.
            format : str, optional
                Image format: png, jpg, eps, ...
            dpi : int, optional
                Dots per inch for saved image."""
        if len(suffix):
            suffix = '_' + suffix
        if not interactive:
            import matplotlib as mpl
            mpl.use('Agg')
            save = True
        from matplotlib import pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D
        x = self.x[self.v[0]]
        if self.roots is None:
            self.root()
        fig = plt.figure(figsize=(7., 7.), facecolor='w')
        ax = fig.add_axes([0.05, 0.05, 0.85, 0.85], projection='3d')
        i0 = np.argmax(x >= xlim[0]) if xlim[0] is not None else 0
        i1 = np.argmin(x <= xlim[1]) + 1 if xlim[1] is not None else self.nx
        if logscale:
            for i in range(i0, i1, max(1, step)):
                j0 = np.argmax(self.s[i] >= slim[0]) if slim[0] is not None else 0
                j1 = np.argmin(self.s[i] <= slim[1]) + 1 if slim[1] is not None else self.s[i].size
                xp = x[i] * np.ones(self.s[i][j0:j1].size)
                ax.plot(np.log10(xp), np.log10(self.s[i][j0:j1]), self.roots[i][j0:j1], '-k', lw=0.75)
            ax.set_xlabel('log$_{10}$(%s)' % self.v[0])
            ax.set_ylabel('log$_{10}$(s)')
        else:
            for i in range(i0, i1, max(1, step)):
                j0 = np.argmax(self.s[i] >= slim[0]) if slim[0] is not None else 0
                j1 = np.argmin(self.s[i] <= slim[1]) + 1 if slim[1] is not None else self.s[i].size
                xp = x[i] * np.ones(self.s[i][j0:j1].size)
                ax.plot(xp, self.s[i][j0:j1], self.roots[i][j0:j1], '-k', lw=0.75)
            ax.set_xlabel(self.v[0])
            ax.set_ylabel('s')
        ax.set_zlabel('Q$_{W}$(s, %s)' % self.v[0])
        fig.text(0.9, 0.9, r'$\gamma$ = %g' % self.gamma, ha='right', va='top')
        if save:
            if len(dir) and not os.path.isdir(dir):
                os.makedirs(dir)
            fig.savefig(os.path.join('%s' % dir, 'roots3d_%s_gamma_%g%s.%s' % (self.v[0], self.gamma, suffix, fmt)), dpi=dpi)

        return fig

    def plot_roots_s(self, value_list, interactive=True, save=False, dir='', suffix='', fmt='png', dpi=300):
        """ Plots roots for the given values of \rho (if var == 'rho') or t (if var == 't') as functions of s.
            Returns matplotlib figure
            value_list : iterable
                List of \rho (or t) values within the rholim (or tlim) interval for which the roots will be plotted
            interactive : bool, optional
                If true, creates interactive figure. Set to False for servers without X.
            save : bool, optional
                Save the image or not.
            dir : str, optional
                Directory where to save the image.
            suffix : str, optional
                Adds this string to image filename.
            format : str, optional
                Image format: png, jpg, eps, ...
            dpi : int, optional
                Dots per inch for saved image."""
        if len(suffix):
            suffix = '_' + suffix
        if not interactive:
            import matplotlib as mpl
            mpl.use('Agg')
            save = True
        from matplotlib import pyplot as plt
        x = self.x[self.v[0]]
        if self.roots is None:
            self.root()
        fig = plt.figure(figsize=(6., 4.), facecolor='w')
        ax = fig.add_axes([0.12, 0.13, 0.86, 0.8])
        ls = ['-', '--', ':', '-.']
        lw = [1.5, 1., 2., 1.]
        iclist = []
        for ival, val in enumerate(value_list):
            if not x[0] <= val <= x[-1]:
                print('Plotting warning: value %g is out of the range (%g, %g)' % (val, x[0], x[-1]))
                continue
            ic = np.argmin(np.abs(val - x))  # index of the closest value
            if ic in iclist:
                continue
            iclist.append(ic)
            ax.plot(self.s[ic], self.roots[ic], label='%g' % x[ic], ls=ls[ival % 4], lw=lw[ival % 4])
        ax.set_title(r'$\gamma$ = %g' % self.gamma)
        ax.legend(loc=0, frameon=False, ncol=len(value_list) // 6 + 1, title=self.v[0])
        if self.log_s:
            ax.set_xscale('log')
            ax.set_yscale('log')
        ax.set_xlabel('s')
        ax.set_ylabel('Q$_{W}$(s)')
        if save:
            if len(dir) and not os.path.isdir(dir):
                os.makedirs(dir)
            fig.savefig(os.path.join('%s' % dir, 'roots_s_%s_gamma_%g_%s%s.%s' % (self.v[0], self.gamma,
                                                                                  '_'.join(['%g' % val for val in value_list]),
                                                                                  suffix, fmt)), dpi=dpi)

        return fig

    def plot_roots_s_all(self, normalize=False, xlim=(None, None), slim=(None, None), interactive=True, save=False,
                         dir='', suffix='', fmt='png', dpi=300):
        """ Plots all roots as functions of s.
            Returns matplotlib figure
            normalize : bool, optional
                If true, normalizes all roots to rho-average (or t-average) root.
            xlim : tuple of two floats, optional
                Plot roots only in the interval xlim[0] < rho (or t) < xlim[1]
            slim : tuple of 2 floats, optional
                Plot roots only in the interval slim[0] < s < slim[1]
            interactive : bool, optional
                If true, creates interactive figure. Set to False for servers without X.
            save : bool, optional
                Save the image or not.
            dir : str, optional
                Directory where to save the image.
            suffix : str, optional
                Adds this string to image filename.
            format : str, optional
                Image format: png, jpg, eps, ...
            dpi : int, optional
                Dots per inch for saved image."""
        if len(suffix):
            suffix = '_' + suffix
        if not interactive:
            import matplotlib as mpl
            mpl.use('Agg')
            save = True
        from matplotlib import pyplot as plt
        x = self.x[self.v[0]]
        if self.roots is None:
            self.root()
        fig = plt.figure(figsize=(6., 4.), facecolor='w')
        ax = fig.add_axes([0.13, 0.13, 0.85, 0.8])
        if normalize:
            root0i = interp1d(self.root0[:, 0], self.root0[:, 1], bounds_error=False, fill_value=(self.root0[0, 1], self.root0[-1, 1]))
            suffix = '_normalized' + suffix
            ylabel = 'Q$_{W}$(s, %s)/{Q$_{W}$}$_{av}$(s)' % self.v[0]
        else:
            ylabel = 'Q$_{W}$(s, %s)' % self.v[0]
        i0 = np.argmax(x >= xlim[0]) if xlim[0] is not None else 0
        i1 = np.argmin(x <= xlim[1]) if xlim[1] is not None else self.nx - 1
        j00 = np.argmax(self.s[i0] >= slim[0]) if slim[0] is not None else 0
        j10 = np.argmin(self.s[i0] <= slim[1]) + 1 if slim[1] is not None else self.s[i0].size
        j01 = np.argmax(self.s[i1] >= slim[0]) if slim[0] is not None else 0
        j11 = np.argmin(self.s[i1] <= slim[1]) + 1 if slim[1] is not None else self.s[i1].size
        if normalize:
            l0, = ax.plot(self.s[i0][j00:j10], self.roots[i0][j00:j10] / root0i(self.s[i0][j00:j10]), ls='-', color='C0', lw=1.0, zorder=x.size + 1)
            l1, = ax.plot(self.s[i1][j01:j11], self.roots[i1][j01:j11] / root0i(self.s[i1][j01:j11]), ls='-', color='C1', lw=1.0, zorder=x.size + 2)
        else:
            l0, = ax.plot(self.s[i0][j00:j10], self.roots[i0][j00:j10], ls='-', color='C0', lw=1.0, zorder=x.size + 1)
            l1, = ax.plot(self.s[i1][j01:j11], self.roots[i1][j01:j11], ls='-', color='C1', lw=1.0, zorder=x.size + 2)
        for i in range(i0 + 1, i1):
            j0 = np.argmax(self.s[i] >= slim[0]) if slim[0] is not None else 0
            j1 = np.argmin(self.s[i] <= slim[1]) + 1 if slim[1] is not None else self.s[i].size
            if normalize:
                ax.plot(self.s[i][j0:j1], self.roots[i][j0:j1] / root0i(self.s[i][j0:j1]), '-k', lw=0.5, alpha=0.25)
            else:
                ax.plot(self.s[i][j0:j1], self.roots[i][j0:j1], '-k', lw=0.5, alpha=0.25)
        ax.legend([l0, l1], ['%s = %g' % (self.v[0], x[i0]), '%s = %g' % (self.v[0], x[i1])], loc=0, frameon=False)
        ax.set_title(r'$\gamma$ = %g' % self.gamma)
        if self.log_s:
            ax.set_xscale('log')
            if not normalize:
                ax.set_yscale('log')
        ax.set_xlabel('s')
        ax.set_ylabel(ylabel)
        if save:
            if len(dir) and not os.path.isdir(dir):
                os.makedirs(dir)
            fig.savefig(os.path.join('%s' % dir, 'roots_s_all_%s_%g-%g_gamma_%g%s.%s' % (self.v[0], x[i0], x[i1],
                                                                                         self.gamma, suffix, fmt)), dpi=dpi)

        return fig

    def plot_automodel_err(self, xlim=(None, None), slim=(None, None), show_front=True, interactive=True, save=False,
                           dir='', suffix='', fmt='png', dpi=300):
        """ Plots errors of the automodel solutions.
            Returns matplotlib figure
            xlim : tuple of two floats, optional
                Plot roots only in the interval xlim[0] < rho (or t) < xlim[1]
            interactive : bool, optional
                If true, creates interactive figure. Set to False for servers without X.
            save : bool, optional
                Save the image or not.
            dir : str, optional
                Directory where to save the image.
            suffix : str, optional
                Adds this string to image filename.
            format : str, optional
                Image format: png, jpg, eps, ...
            dpi : int, optional
                Dots per inch for saved image."""
        if len(suffix):
            suffix = '_' + suffix
        if not interactive:
            import matplotlib as mpl
            mpl.use('Agg')
            save = True
        from matplotlib import pyplot as plt
        x = self.x[self.v[0]]
        if self.auts is None:
            self.automodel_sol()
        fig = plt.figure(figsize=(6., 4.), facecolor='w')
        ax = fig.add_axes([0.13, 0.13, 0.85, 0.8])
        i0 = np.argmax(x >= xlim[0]) if xlim[0] is not None else 0
        i1 = np.argmin(x <= xlim[1]) if xlim[1] is not None else x.size - 1
        j00 = np.argmax(self.s[i0] >= slim[0]) if slim[0] is not None else 0
        j10 = np.argmin(self.s[i0] <= slim[1]) + 1 if slim[1] is not None else self.s[i0].size
        j01 = np.argmax(self.s[i1] >= slim[0]) if slim[0] is not None else 0
        j11 = np.argmin(self.s[i1] <= slim[1]) + 1 if slim[1] is not None else self.s[i1].size
        l0, = ax.plot(self.x[self.v[1]][i0][j00:j10], self.auts[i0][j00:j10] / self.es[i0][j00:j10], ls='-', color='C0', lw=1.0, zorder=self.nx + 1)
        l1, = ax.plot(self.x[self.v[1]][i1][j01:j11], self.auts[i1][j01:j11] / self.es[i1][j01:j11], ls='-', color='C1', lw=1.0, zorder=self.nx + 2)
        for i in range(i0 + 1, i1):
            j0 = np.argmax(self.s[i] >= slim[0]) if slim[0] is not None else 0
            j1 = np.argmin(self.s[i] <= slim[1]) + 1 if slim[1] is not None else self.s[i].size
            ax.plot(self.x[self.v[1]][i][j0:j1], self.auts[i][j0:j1] / self.es[i][j0:j1], '-k', lw=0.5, alpha=0.25)
        if not self.rho and show_front:
            rho_front = []
            func_front = []
            for i in range(self.nx):
                indx, = np.where(self.s[i] > 1)
                if indx.size:
                    if indx[0]:
                        rho_front.append(self.x['rho'][i][indx[0] - 1])
                        func_front.append((self.auts[i] / self.es[i])[indx[0] - 1])
            ax.plot(rho_front, func_front, '.', color='r', zorder=self.nx + 3)
        ax.set_xlabel('$t$' if self.rho else r'$\rho$')
        ax.legend([l0, l1], ['%s = %g' % (self.v[0], x[i0]), '%s = %g' % (self.v[0], x[i1])], loc=0, frameon=False)
        ax.set_title(r'$\gamma$ = %g' % self.gamma)
        ax.set_xscale('log')
        ax.set_ylabel(r'$f_{auto}(\rho, t)$/$f_{exact}(\rho, t)$')
        if save:
            if len(dir) and not os.path.isdir(dir):
                os.makedirs(dir)
            fig.savefig(os.path.join('%s' % dir, 'auto_err_%s_%g-%g_gamma_%g%s.%s' % (self.v[0], x[i0], x[i1],
                                                                                      self.gamma, suffix, fmt)), dpi=dpi)

        return fig

    def plot_roots_proj(self, step=1, xnorm=None, xlim=(None, None), interactive=True, save=False, dir='', suffix='', fmt='png', dpi=300):
        """ Plots normalized self similar functions (roots) in the 2D projection.
            Returns matplotlib figure
            step : int, optional
                If specified, only each i-th == step self similar function will be plotted
            xnorm : float or None, optional
                If specified, all roots, Q(x,s), will be normalized to Q(xnorm,s).
                If not specified, Q(x,s) will be normalized to x-averaged Q_av(s).
            xlim : tuple of two floats, optional
                Plot roots only in the interval xlim[0] < rho (or t) < xlim[1]
            interactive : bool, optional
                If true, creates interactive figure. Set to False for servers without X.
            save : bool, optional
                Save the image or not.
            dir : str, optional
                Directory where to save the image.
            suffix : str, optional
                Adds this string to image filename.
            format : str, optional
                Image format: png, jpg, eps, ...
            dpi : int, optional
                Dots per inch for saved image."""
        if len(suffix):
            suffix = '_' + suffix
        if not interactive:
            import matplotlib as mpl
            mpl.use('Agg')
            save = True
        from matplotlib import pyplot as plt
        x = self.x[self.v[0]]
        if self.roots is None:
            self.root()
        if xnorm is None:
            ylabel = 'Q$_{W}$(s, %s)/{Q$_{W}$}$_{av}$(s)' % self.v[0]
            root0i = interp1d(self.root0[:, 0], self.root0[:, 1], bounds_error=False, fill_value=(self.root0[0, 1], self.root0[-1, 1]))
        else:
            smin = 1e9
            smax = 0
            for i in range(x.size):
                smin = min(smin, self.s[i][0])
                smax = max(smax, self.s[i][-1])
            ind_sorted = np.argsort(np.abs(x - xnorm))
            root0 = None
            x0 = None
            for i in ind_sorted:
                if abs(self.s[i][0] - smin) < 1.e-6 and abs(self.s[i][-1] - smax) < 1.e-6:
                    root0 = self.roots[i]
                    s0 = self.s[i]
                    x0 = x[i]
                    break
            if root0 is None:
                print('Error: none of the roots is defined in the (%.3f, %.3f) interval' % (smin, smax))
                return None
            ylabel = 'Q$_{W}$(s, %s)/Q$_{W}$(s, %s = %.1f)' % (self.v[0], self.v[0], x0)
            suffix = '_%snorm_%.1f' % (self.v[0], x0) + suffix
            root0i = interp1d(s0, root0, bounds_error=False, fill_value=(root0[0], root0[-1]))
        fig = plt.figure(figsize=(6., 4.), facecolor='w')
        ax = fig.add_axes([0.14, 0.13, 0.84, 0.8])
        i0 = np.argmax(x >= xlim[0]) if xlim[0] is not None else 0
        i1 = np.argmin(x <= xlim[1]) if xlim[1] is not None else x.size - 1
        for i in range(i0, i1 + 1, max(1, step)):
            root_norm = self.roots[i] / root0i(self.s[i])
            ax.scatter(np.ones(root_norm.size) * x[i], root_norm, s=0.75, c='k', alpha=0.35)
        ax.set_xscale('log')
        ax.set_xlabel(self.v[0])
        ax.set_ylabel(ylabel)
        ax.set_title(r'$\gamma$ = %g' % self.gamma)
        if save:
            if len(dir) and not os.path.isdir(dir):
                os.makedirs(dir)
            fig.savefig(os.path.join('%s' % dir, 'roots_proj_%s_%g-%g_gamma_%g%s.%s' % (self.v[0], x[i0], x[i1],
                                                                                        self.gamma, suffix, fmt)), dpi=dpi)

        return fig

    def plot_automodel_err_3d(self, step=1, xlim=(None, None), slim=(None, None), show_front=False, logscale=True, interactive=True, save=False,
                              dir='', suffix='', fmt='png', dpi=300):
        """ Plots the calculated self similar functions (roots) in the 3D space.
            Returns matplotlib figure
            step : int, optional
                If specified, only each i-th == step self similar function will be plotted
            xlim : tuple of 2 floats, optional
                Plot roots only in the interval xlim[0] < rho (or t) < xlim[1]
            slim : tuple of 2 floats, optional
                Plot roots only in the interval slim[0] < s < slim[1]
            interactive : bool, optional
                If true, creates interactive figure. Set to False for servers without X.
            save : bool, optional
                Save the image or not.
            dir : str, optional
                Directory where to save the image.
            suffix : str, optional
                Adds this string to image filename.
            format : str, optional
                Image format: png, jpg, eps, ...
            dpi : int, optional
                Dots per inch for saved image."""
        if len(suffix):
            suffix = '_' + suffix
        if not interactive:
            import matplotlib as mpl
            mpl.use('Agg')
            save = True
        from matplotlib import pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D
        x = self.x[self.v[0]]
        if self.roots is None:
            self.root()
        fig = plt.figure(figsize=(7., 7.), facecolor='w')
        ax = fig.add_axes([0.05, 0.05, 0.85, 0.85], projection='3d')
        if xlim[0] is None and xlim[1] is None:
            i0 = 0
            i1 = self.nx
        else:
            if xlim[0] is not None and xlim[1] is not None:
                indx, = np.where((x >= xlim[0]) * (x <= xlim[1]))
            elif xlim[0] is not None:
                indx, = np.where(x >= xlim[0])
            else:
                indx, = np.where(x <= xlim[1])
            if not indx.size:
                print('Error: wrong interval for %s is specified' % self.v[0])
                return None
            i0 = indx[0]
            i1 = indx[-1] + 1
        rho_front = []
        t_front = []
        func_front = []
        for i in range(i0, i1, max(1, step)):
            if slim[0] is None and slim[1] is None:
                j0 = 0
                j1 = self.s[i].size
            else:
                if slim[0] is not None and slim[1] is not None:
                    indx, = np.where((self.s[i] >= slim[0]) * (self.s[i] <= slim[1]))
                elif slim[0] is not None:
                    indx, = np.where(self.s[i] >= slim[0])
                else:
                    indx, = np.where(self.s[i] <= slim[1])
                if not indx.size:
                    continue
                j0 = indx[0]
                j1 = indx[-1] + 1
            if not self.rho and show_front:
                indx, = np.where(self.s[i] > 1)
                if indx.size:
                    if j0 <= indx[0] < j1:
                        t_front.append(self.x['t'][i])
                        rho_front.append(self.x['rho'][i][indx[0] - 1])
                        func_front.append((self.auts[i] / self.es[i])[indx[0] - 1])
            if logscale:
                xp = x[i] * np.ones(self.s[i][j0:j1].size)
                ax.plot(np.log10(xp), np.log10(self.x[self.v[1]][i][j0:j1]), np.minimum(10., self.auts[i][j0:j1] / self.es[i][j0:j1]), '-k', lw=0.75)
            else:
                xp = x[i] * np.ones(self.s[i][j0:j1].size)
                ax.plot(xp, self.x[self.v[1]][i][j0:j1], np.minimum(10., self.auts[i][j0:j1] / self.es[i][j0:j1]), '-k', lw=0.75)
        if logscale:
            if not self.rho and show_front:
                ax.plot(np.log10(t_front), np.log10(rho_front), func_front, '.', color='r', zorder=self.nx + 3)
            ax.set_xlabel('log$_{10}$(%s)' % self.v[0])
            ax.set_ylabel(r'log$_{10}$($\%s$)' % self.v[1])
        else:
            if not self.rho and show_front:
                ax.plot(t_front, rho_front, func_front, '.', color='r', zorder=self.nx + 3)
            ax.set_xlabel(self.v[0])
            ax.set_ylabel('s')
        ax.set_zlabel(r'$f_{auto}(\rho, t)$/$f_{exact}(\rho, t)$')
        ax.set_zlim(0, 2.)
        fig.text(0.9, 0.9, r'$\gamma$ = %g' % self.gamma, ha='right', va='top')
        if save:
            if len(dir) and not os.path.isdir(dir):
                os.makedirs(dir)
            fig.savefig(os.path.join('%s' % dir, 'auto3d_%s_gamma_%g%s.%s' % (self.v[0], self.gamma, suffix, fmt)), dpi=dpi)

        return fig


if __name__ == "__main__":
    parser = MakeParser()
    args = parser.parse_args()
    kwargs = vars(args)
    if 'filename' not in kwargs:
        for key in kwargs.keys():
            print(key, kwargs[key])
    selfsim = SelfSimVoigt(**kwargs)
    selfsim.automodel_sol()
    selfsim.save(extended=False, suffix='Voigt', dir='path/to/data')

    quit()
