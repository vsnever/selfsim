# -*- coding: utf-8 -*-
from __future__ import print_function
import pyopencl as cl


def get_flops(device, show=True):
    comp_units = device.get_info(cl.device_info.MAX_COMPUTE_UNITS)
    gpu_clock = device.get_info(cl.device_info.MAX_CLOCK_FREQUENCY)
    vendor = device.get_info(cl.device_info.VENDOR).lower()
    gflops = 0
    if "nvidia" in vendor:
        cc_maj = device.get_info(cl.device_info.COMPUTE_CAPABILITY_MAJOR_NV)
        cc_min = device.get_info(cl.device_info.COMPUTE_CAPABILITY_MINOR_NV)
        alu_lanes = 128
        if cc_maj == 1:
            alu_lanes = 8
        elif cc_maj == 2:
            alu_lanes = 32 if cc_min == 0 else 48
        elif cc_maj == 3:
            alu_lanes = 192
        elif cc_maj == 5:
            alu_lanes = 128
        elif cc_maj == 6:
            alu_lanes = 64 if cc_min == 0 else 128
        elif cc_maj == 7:
            alu_lanes = 64
        gflops = comp_units * alu_lanes * 2 * gpu_clock / 1000.
    elif "amd" in vendor or "advanced" in vendor:
        try:
            ww = device.get_info(cl.device_info.WAVEFRONT_WIDTH_AMD)
        except:
            ww = 64
        gflops = comp_units * ww * 2 * gpu_clock / 1000.
    elif "intel" in vendor:
        gflops = comp_units * 16 * gpu_clock / 1000.
    elif "arm" in vendor:
        gflops = comp_units * 2 * 16 * gpu_clock / 1000.
    else:
        print("Error: unsupported device vendor: %s" % vendor)
    if show:
        print("Number of compute units: %d" % comp_units)
        print("GPU maximum clock rate: %d" % gpu_clock)
        if gflops:
            print("Theoretical peak performance: %g GFLOPS" % gflops)
        else:
            print("Error: unable to estimate theoretical peak performance")

    return gflops


def get_best_gpu(platforms=None, device_type=cl.device_type.GPU | cl.device_type.ACCELERATOR, show=True):
    if device_type == cl.device_type.DEFAULT:
        device_type = cl.device_type.GPU | cl.device_type.ACCELERATOR
    if not (cl.device_type.GPU | cl.device_type.ACCELERATOR) & device_type:
        print("\nError: this function works with GPU devices only\n")
        return (None, None)
    if platforms is None:
        platforms = cl.get_platforms()
    if show:
        print("Selecting best GPU")
    device_best = None
    max_gflops = 0
    for iplat, platform in enumerate(platforms):
        if show:
            print("\nOpenCL platform %d: %s" % (iplat, platform.get_info(cl.platform_info.NAME)))
        devices = platform.get_devices(device_type=device_type)
        for idev, device in enumerate(devices):
            if show:
                print("\nDevice %d: %s %s" % (idev, device.get_info(cl.device_info.VENDOR), device.get_info(cl.device_info.NAME)))
            gflops = get_flops(device, show)
            if gflops > max_gflops:
                device_best = device
    if device_best is None:
        print("\nError: no supported GPUs found\n")
        return (None, None)
    if show:
        print("\nSelected device: %s %s\n" % (device_best.get_info(cl.device_info.VENDOR), device_best.get_info(cl.device_info.NAME)))
    return (device_best, max_gflops)


def get_first_device(platforms=None, device_type=cl.device_type.GPU | cl.device_type.ACCELERATOR, show=True):
    if platforms is None:
        platforms = cl.get_platforms()
    for platform in platforms:
        devices = platform.get_devices(device_type=device_type)
        if len(devices):
            device = devices[0]
            if show:
                print("Selected OpenCL device: %s %s\n" % (device.get_info(cl.device_info.VENDOR), device.get_info(cl.device_info.NAME)))
            return device
    print("\nError: there are no devices of specified type\n")

    return None


def device_select(platfrom_id=None, device_id=None, device_type=cl.device_type.GPU | cl.device_type.ACCELERATOR, show=True):
    platforms = cl.get_platforms()
    n_platforms = len(platforms)
    if device_type == cl.device_type.DEFAULT:
        device_type = cl.device_type.GPU | cl.device_type.ACCELERATOR
    non_gpu_device = not (cl.device_type.GPU | cl.device_type.ACCELERATOR) & device_type
    if platfrom_id < 0:
        platfrom_id = None
    if device_id < 0:
        device_id = None
    if platfrom_id is None:
        if non_gpu_device:
            return (get_first_device(platforms, device_type, show), 0)
        return get_best_gpu(platforms, device_type, show)
    if platfrom_id < n_platforms:
        platform = platforms[platfrom_id]
        devices = platform.get_devices(device_type=device_type)
        n_devices = len(devices)
        if device_id is None:
            if non_gpu_device:
                return (get_first_device([platform], device_type, show), 0)
            return get_best_gpu([platform], device_type, show)
        if device_id < n_devices:
            device = devices[device_id]
            if show:
                print("Selected OpenCL device: %s %s" % (device.get_info(cl.device_info.VENDOR), device.get_info(cl.device_info.NAME)))
            if non_gpu_device:
                return (device, 0)
            return (device, get_flops(device, show))
        print("\nWarning: %s platform has %d devices of specified type\n" % (platform.get_info(cl.platform_info.NAME), n_platforms))
        if non_gpu_device:
            return (get_first_device([platform], device_type, show), 0)
        return get_best_gpu([platform], device_type, show)
    print("\nWarning: system has only %d OpenCL platforms\n" % n_platforms)
    if non_gpu_device:
        return (get_first_device(platforms, device_type, show), 0)

    return get_best_gpu(platforms, device_type, show)


if __name__ == "__main__":
    device_select(platfrom_id=-1, device_id=-1, device_type=cl.device_type.ACCELERATOR, show=True)
