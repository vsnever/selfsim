#ifndef BlockSize
#define BlockSize 256
#endif
#ifndef FP
#define FP float //float or double
#endif
#ifndef N_COS
#define N_COS 512
#endif
#ifndef N_EXP
#define N_EXP 8192
#endif
#ifndef N_MAX
#define N_MAX 536870912
#endif
#define PI 3.14159265358979323846264338
#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#define MAX(a,b) (((a) > (b)) ? (a) : (b))
#define BOOL(x) ((x) ? 1 : 0)

__kernel void inner_int_large_p(__global FP * const restrict res, const __global FP * const restrict p, 
                                const unsigned int ip0, const FP gamma_p2, const FP xpow_min, const FP xpow_max, const unsigned int nx){
    const unsigned int tid = get_local_id(0);
    const unsigned int nxt = nx / BlockSize + BOOL(nx % BlockSize);
    const unsigned int ix_stop = MIN(nxt * (tid + 1), nx);
    const FP pb = p[ip0 + get_group_id(0)];
    __local FP res_loc[BlockSize];
    res_loc[tid] = 0;
    const FP dxpow = (xpow_max - xpow_min) / nx;
    FP xprev = powr(10, xpow_min + nxt * tid * dxpow);
    FP fprev = sin(pb * xprev) / native_powr(1 + xprev, gamma_p2);  
    for (unsigned int ix = nxt * tid + 1; ix <= ix_stop; ix++){
        FP x = powr(10, xpow_min + ix * dxpow);
        FP f = sin(pb * x) / native_powr(1 + x, gamma_p2);
        res_loc[tid] += (f + fprev) * (x - xprev);
        xprev = x;
        fprev = f;
    }
    res_loc[tid] *= 0.5;
    barrier(CLK_LOCAL_MEM_FENCE);
    if (BlockSize >= 256) {
        if (tid < 128) res_loc[tid] += res_loc[128 + tid];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (BlockSize >= 128) {
        if (tid < 64) res_loc[tid] += res_loc[64 + tid];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (BlockSize >= 64) {
        if (tid < 32) res_loc[tid] += res_loc[32 + tid];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (BlockSize >= 32) {
        if (tid < 16) res_loc[tid] += res_loc[16 + tid];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (BlockSize >= 16) {
        if (tid < 8) res_loc[tid] += res_loc[8 + tid];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (tid < 4) res_loc[tid] += res_loc[4 + tid];
    barrier(CLK_LOCAL_MEM_FENCE);
    if (tid < 2) res_loc[tid] += res_loc[2 + tid];
    barrier(CLK_LOCAL_MEM_FENCE);
    if (!tid) res[ip0 + get_group_id(0)] = (res_loc[0] + res_loc[1]);
}

__kernel void inner_int_small_p(__global FP * const restrict res, const __global FP * const restrict p, 
                                const unsigned int ip0, const FP gamma, const FP xmax, const unsigned int nx){
    const unsigned int tid = get_local_id(0);
    const unsigned int nxt = nx / BlockSize + BOOL(nx % BlockSize);
    const unsigned int ix_stop = MIN(nxt * (tid + 1), nx);
    const FP pb = p[ip0 + get_group_id(0)];
    __local FP res_loc[BlockSize];  
    const FP dx = xmax / nx;
    res_loc[tid] = 0.5 * (sin(pb * nxt * tid * dx) / native_powr(1 + nxt * tid * dx, gamma) +
                    sin(pb * ix_stop * dx) / native_powr(1 + ix_stop * dx, gamma));
    for (unsigned int ix = nxt * tid + 1; ix < ix_stop; ix++){
        res_loc[tid] += sin(pb * ix * dx) / native_powr(1 + ix * dx, gamma);
    }
    barrier(CLK_LOCAL_MEM_FENCE);
    if (BlockSize >= 256) {
        if (tid < 128) res_loc[tid] += res_loc[128 + tid];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (BlockSize >= 128) {
        if (tid < 64) res_loc[tid] += res_loc[64 + tid];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (BlockSize >= 64) {
        if (tid < 32) res_loc[tid] += res_loc[32 + tid];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (BlockSize >= 32) {
        if (tid < 16) res_loc[tid] += res_loc[16 + tid];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (BlockSize >= 16) {
        if (tid < 8) res_loc[tid] += res_loc[8 + tid];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (tid < 4) res_loc[tid] += res_loc[4 + tid];
    barrier(CLK_LOCAL_MEM_FENCE);
    if (tid < 2) res_loc[tid] += res_loc[2 + tid];
    barrier(CLK_LOCAL_MEM_FENCE);
    if (!tid) res[ip0 + get_group_id(0)] = dx * (res_loc[0] + res_loc[1]) * pb;
}

__kernel void exact_sol_t_int(__global FP * const restrict res, const __global FP * const restrict q_inner,
                              const __global FP * const restrict rho, const FP t, const FP plim, FP dp, 
                              const FP p_pow_min, const FP dp_pow, const FP gamma, const FP coeff_small_p){
    const unsigned int tid = get_local_id(0);
    const FP rwo_b = rho[get_group_id(0)];
    dp = MIN(dp, 0.5 * PI / (rwo_b * N_COS));
    const unsigned int np = MIN(N_MAX, (unsigned int) (plim / dp) + 1);
    dp = plim / np;
    const FP pmin = powr(10, p_pow_min);
    __local FP res_loc[BlockSize];
    res_loc[tid] = 0;
    for (unsigned int ip = tid; ip < np; ip += BlockSize){
        const FP p = ip * dp;
        FP q = 0;
        if (p > pmin){
            const unsigned int iq = (unsigned int) ((native_log10(p) - p_pow_min) / dp_pow);
            const FP p0 = native_powr(10, p_pow_min + iq * dp_pow);
            const FP p1 = native_powr(10, p_pow_min + (iq + 1) * dp_pow);
            const FP q0 = q_inner[iq];
            q = q0 + (q_inner[iq+1] - q0) * (p - p0) / (p1 - p0);
        }
        else q = coeff_small_p * powr(p, gamma);
        res_loc[tid] += native_cos(p * rwo_b) * native_exp(-t * q);
    }
    barrier(CLK_LOCAL_MEM_FENCE);
    if (BlockSize >= 256) {
        if (tid < 128) res_loc[tid] += res_loc[128 + tid];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (BlockSize >= 128) {
        if (tid < 64) res_loc[tid] += res_loc[64 + tid];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (BlockSize >= 64) {
        if (tid < 32) res_loc[tid] += res_loc[32 + tid];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (BlockSize >= 32) {
        if (tid < 16) res_loc[tid] += res_loc[16 + tid];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (BlockSize >= 16) {
        if (tid < 8) res_loc[tid] += res_loc[8 + tid];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (tid < 4) res_loc[tid] += res_loc[4 + tid];
    barrier(CLK_LOCAL_MEM_FENCE);
    if (tid < 2) res_loc[tid] += res_loc[2 + tid];
    barrier(CLK_LOCAL_MEM_FENCE);
    if (!tid) res[get_group_id(0)] = dp * (res_loc[0] + res_loc[1] - 0.5);
}

__kernel void exact_sol_rho_int(__global FP * const restrict res, const __global FP * const restrict q_inner,
                                const __global FP * const restrict t, const __global FP * const restrict plim, 
                                const FP rho, FP dp, const FP p_pow_min, const FP dp_pow, const FP gamma, const FP coeff_small_p){
    const unsigned int tid = get_local_id(0);
    const FP tb = t[get_group_id(0)];
    const FP plimb = plim[get_group_id(0)];
    dp = MIN(dp, plimb / N_EXP);
    const unsigned int np = MIN(N_MAX, (unsigned int) (plimb / dp) + 1);
    dp = plimb / np;
    const FP pmin = powr(10, p_pow_min);
    __local FP res_loc[BlockSize];
    res_loc[tid] = 0;
    for (unsigned int ip = tid; ip < np; ip += BlockSize){
        const FP p = ip * dp;
        FP q = 0;
        if (p > pmin){
            const unsigned int iq = (unsigned int) ((native_log10(p) - p_pow_min) / dp_pow);
            const FP p0 = native_powr(10, p_pow_min + iq * dp_pow);
            const FP p1 = native_powr(10, p_pow_min + (iq + 1) * dp_pow);
            const FP q0 = q_inner[iq];
            q = q0 + (q_inner[iq+1] - q0) * (p - p0) / (p1 - p0);
        }
        else q = coeff_small_p * powr(p, gamma);
        res_loc[tid] += native_cos(p * rho) * native_exp(-tb * q);
    }
    barrier(CLK_LOCAL_MEM_FENCE);
    if (BlockSize >= 256) {
        if (tid < 128) res_loc[tid] += res_loc[128 + tid];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (BlockSize >= 128) {
        if (tid < 64) res_loc[tid] += res_loc[64 + tid];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (BlockSize >= 64) {
        if (tid < 32) res_loc[tid] += res_loc[32 + tid];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (BlockSize >= 32) {
        if (tid < 16) res_loc[tid] += res_loc[16 + tid];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (BlockSize >= 16) {
        if (tid < 8) res_loc[tid] += res_loc[8 + tid];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (tid < 4) res_loc[tid] += res_loc[4 + tid];
    barrier(CLK_LOCAL_MEM_FENCE);
    if (tid < 2) res_loc[tid] += res_loc[2 + tid];
    barrier(CLK_LOCAL_MEM_FENCE);
    if (!tid) res[get_group_id(0)] = dp * (res_loc[0] + res_loc[1] - 0.5);
}
