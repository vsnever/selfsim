#ifndef BlockSize
#define BlockSize 256
#endif
#ifndef FP
#define FP float // float or double
#endif
#ifndef N_COS
#define N_COS 512
#endif
#ifndef N_EXP
#define N_EXP 8192
#endif
#if FP == double
#define FP4 double4
#define PI 3.14159265358979323846264338
#else
#define FP4 float4
#define PI 3.141592654f
#endif
#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#define MAX(a,b) (((a) > (b)) ? (a) : (b))
#define BOOL(x) ((x) ? 1 : 0)


__kernel void zero_buffer(__global FP * const buff, const unsigned int buff_size){
    const unsigned int i = get_group_id(0) * BlockSize + get_local_id(0);
    if (i < buff_size) buff[get_group_id(0) * BlockSize + get_local_id(0)] = 0;
}

__kernel void inner_int_small_p(__global FP * const restrict q, __global FP * const restrict w,
                                __global FP * const restrict omega, const FP p_pow_min,
                                const FP dp_pow, const FP w0, const FP c_a,
                                const unsigned int ip_end, const unsigned int iw_begin, const unsigned int iw_end){
    const unsigned int tid = get_local_id(0);
    const unsigned int ip = get_group_id(0) * BlockSize + tid;
    __local FP w_loc[BlockSize];
    __local FP om_loc[BlockSize];
    __local FP dev_w[BlockSize];
    __local FP dev_w3[BlockSize];
    FP q_loc1 = 0, q_loc2 = 0, q_loc3 = 0, q_loc4 = 0;
    const FP p = native_powr(10, p_pow_min + ip * dp_pow);
    const FP w0p = p * w0;
    for (unsigned int iw = iw_begin; iw < iw_end; iw += BlockSize - 1){
        if (iw + tid < iw_end) {
            w_loc[tid] = w[iw + tid];
            om_loc[tid] = omega[iw + tid];
            dev_w[tid] = 1 / w_loc[tid];
            dev_w3[tid] = dev_w[tid] * dev_w[tid] * dev_w[tid];
        }
        barrier(CLK_LOCAL_MEM_FENCE);
        const unsigned int iw1_end = MIN(BlockSize, iw_end - iw) - 1;
        if (w0p / w_loc[0] > 1.e-2) {
            FP f0 = w_loc[0] * w_loc[0] * atan(w0p / w_loc[0]);
            FP f1 = 0;
            for (unsigned int iw1 = 0; iw1 < iw1_end; iw1++){
                f1 = w_loc[iw1 + 1] * w_loc[iw1 + 1] * atan(w0p / w_loc[iw1 + 1]);
                q_loc1 -= (f0 + f1) * (om_loc[iw1 + 1] - om_loc[iw1]);
                f0 = f1;
                q_loc2 += (w_loc[iw1 + 1] + w_loc[iw1]) * (om_loc[iw1 + 1] - om_loc[iw1]);
            }
        }
        else if (w0p / w_loc[iw1_end - 1] <= 1.e-2) {
            for (unsigned int iw1 = 0; iw1 < iw1_end; iw1++){
                q_loc3 += (dev_w[iw1 + 1] + dev_w[iw1]) * (om_loc[iw1 + 1] - om_loc[iw1]);
                q_loc4 -= (dev_w3[iw1 + 1] + dev_w3[iw1]) * (om_loc[iw1 + 1] - om_loc[iw1]);
            }
        }
        else {
            for (unsigned int iw1 = 0; iw1 < iw1_end; iw1++){
                if (w0p / w_loc[iw1] > 1.e-2) {
                    q_loc1 -= (w_loc[iw1 + 1] * w_loc[iw1 + 1] * atan(w0p / w_loc[iw1 + 1]) +\
                               w_loc[iw1] * w_loc[iw1] * atan(w0p / w_loc[iw1])) * (om_loc[iw1 + 1] - om_loc[iw1]);
                    q_loc2 += (w_loc[iw1 + 1] + w_loc[iw1]) * (om_loc[iw1 + 1] - om_loc[iw1]);
                }
                else {
                    q_loc3 += (dev_w[iw1 + 1] + dev_w[iw1]) * (om_loc[iw1 + 1] - om_loc[iw1]);
                    q_loc4 -= (dev_w3[iw1 + 1] + dev_w3[iw1]) * (om_loc[iw1 + 1] - om_loc[iw1]);
                }
            }
        }
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (ip < ip_end)  q[ip] += c_a * (q_loc1 / w0p + q_loc2 + w0p * w0p * q_loc3 / 3 + w0p * w0p * w0p * w0p * q_loc4 / 5);
}


__kernel void inner_int_large_p(__global FP * const restrict q, __global FP * const restrict w,
                                __global FP * const restrict omega, const FP p_pow_min,
                                const FP dp_pow, const FP w0, const FP c_a,
                                const unsigned int ip_begin, const unsigned int ip_end, const unsigned int iw_begin, const unsigned int iw_end){
    const unsigned int tid = get_local_id(0);
    const unsigned int ip = ip_begin + get_group_id(0) * BlockSize + tid;
    __local FP w_loc[BlockSize];
    __local FP om_loc[BlockSize];
    FP q_loc = 0;
    const FP p = native_powr(10, p_pow_min + ip * dp_pow);
    const FP w0p = p * w0;
    for (unsigned int iw = iw_begin; iw < iw_end; iw += BlockSize - 1){
        if (iw + tid < iw_end) {
            w_loc[tid] = w[iw + tid];
            om_loc[tid] = omega[iw + tid];
        }
        barrier(CLK_LOCAL_MEM_FENCE);
        FP f0 = w_loc[0] * w_loc[0] * atan(w0p / w_loc[0]);
        FP f1 = 0;
        for (unsigned int iw1 = 0; iw1 < MIN(BlockSize, iw_end - iw) - 1; iw1++){
            f1 = w_loc[iw1 + 1] * w_loc[iw1 + 1] * atan(w0p / w_loc[iw1 + 1]);
            q_loc += (f1 + f0) * (om_loc[iw1 + 1] - om_loc[iw1]);
            f0 = f1;
        }
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (ip < ip_end) q[ip] += c_a * q_loc / w0p;
}


__kernel void exact_sol_t_rho_int(__global FP * const restrict res, const __global FP * const restrict q_inner,
                                         const unsigned int ires, const FP rho, const FP t, const ulong n_per_group, ulong np_total,
                                         FP dp, const FP p_pow_min, const FP dp_pow, const FP coeff_small_p, const FP pow_small_p){
    const unsigned int tid = get_local_id(0);
    if (!tid) res[ires + get_group_id(0)] = 0;
    const ulong ip_begin = ((ulong) get_group_id(0)) * n_per_group;
    const ulong ip_end = MIN(ip_begin + n_per_group, np_total);
    if (ip_begin >= ip_end) return;
    const FP pmin = powr(10, p_pow_min);
    __local FP res_loc[BlockSize];
    res_loc[tid] = 0;
    for (ulong ip = ip_begin + (ulong) tid; ip < ip_end; ip += BlockSize){
        const FP p = ip * dp;
        FP q = 0;
        if (p > pmin){
            const FP p_pow = native_log10(p);
            const unsigned int iq = (unsigned int) ((p_pow - p_pow_min) / dp_pow);
            const FP p0_pow = p_pow_min + iq * dp_pow;
            const FP q0_pow = native_log10(q_inner[iq]);
            q = native_powr(10, q0_pow + (native_log10(q_inner[iq + 1]) - q0_pow) * (p_pow - p0_pow) / dp_pow);
        }
        else q = coeff_small_p * powr(p, pow_small_p);
        res_loc[tid] += p * native_sin(p * rho) * (native_exp(-t * q) - native_exp(-t));
    }
    barrier(CLK_LOCAL_MEM_FENCE);
    if (BlockSize >= 256) {
        if (tid < 128) res_loc[tid] += res_loc[128 + tid];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (BlockSize >= 128) {
        if (tid < 64) res_loc[tid] += res_loc[64 + tid];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (BlockSize >= 64) {
        if (tid < 32) res_loc[tid] += res_loc[32 + tid];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (BlockSize >= 32) {
        if (tid < 16) res_loc[tid] += res_loc[16 + tid];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (BlockSize >= 16) {
        if (tid < 8) res_loc[tid] += res_loc[8 + tid];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (tid < 4) res_loc[tid] += res_loc[4 + tid];
    barrier(CLK_LOCAL_MEM_FENCE);
    if (tid < 2) res_loc[tid] += res_loc[2 + tid];
    barrier(CLK_LOCAL_MEM_FENCE);
    if (!tid) res[ires + get_group_id(0)] = dp * (res_loc[0] + res_loc[1]) / rho;
}


__kernel void automodel_calc(__global FP * const restrict res, __global FP * const restrict w,
                             __global FP * const restrict d_omega, const FP rho, const FP t, const FP w0, const FP c_a,
                                const unsigned int n_per_group, const unsigned int i_first, const unsigned int i_last){
    const unsigned int tid = get_local_id(0);
    if (!tid) res[get_group_id(0)] = 0;
    const unsigned int i_begin = i_first + get_group_id(0) * n_per_group;
    const unsigned int i_end = MIN(i_begin + n_per_group, i_last);
    if (i_begin >= i_end) return;
    __local FP res_loc[BlockSize];
    res_loc[tid] = 0;
    for (unsigned int i = i_begin + tid; i < i_end; i += BlockSize){
        const FP w_loc = w[i];
        res_loc[tid] += d_omega[i - 1] * w_loc * w_loc * native_exp(- w_loc * rho / w0);
    }
    barrier(CLK_LOCAL_MEM_FENCE);
    if (BlockSize >= 256) {
        if (tid < 128) res_loc[tid] += res_loc[128 + tid];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (BlockSize >= 128) {
        if (tid < 64) res_loc[tid] += res_loc[64 + tid];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (BlockSize >= 64) {
        if (tid < 32) res_loc[tid] += res_loc[32 + tid];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (BlockSize >= 32) {
        if (tid < 16) res_loc[tid] += res_loc[16 + tid];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (BlockSize >= 16) {
        if (tid < 8) res_loc[tid] += res_loc[8 + tid];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (tid < 4) res_loc[tid] += res_loc[4 + tid];
    barrier(CLK_LOCAL_MEM_FENCE);
    if (tid < 2) res_loc[tid] += res_loc[2 + tid];
    barrier(CLK_LOCAL_MEM_FENCE);
    if (!tid) res[get_group_id(0)] = c_a * t * (res_loc[0] + res_loc[1]) / (4 * PI * rho * rho * w0);
}


__kernel void automodel_calc_fine(__global FP * const restrict res, const __global FP4 * const restrict cw,
                                  const FP rho, const FP t, const FP w0, const FP c_a,
                                  const unsigned int n_per_group, unsigned int n_total, const FP dom_fine,
                                  const FP om_min, const FP om_log_min, const unsigned int iom_log_min,
                                  const FP dom_linear, const FP dom_pow){
    const unsigned int tid = get_local_id(0);
    if (!tid) res[get_group_id(0)] = 0;
    const unsigned int i_begin = get_group_id(0) * n_per_group;
    const unsigned int i_end = MIN(i_begin + n_per_group, n_total);
    if (i_begin >= i_end) return;
    __local FP res_loc[BlockSize];
    res_loc[tid] = 0;
    const FP om_pow_min = native_log10(om_log_min);
    for (unsigned int i = i_begin + tid; i < i_end; i += BlockSize){
        const FP om = om_min + i * dom_fine;
        unsigned int iom = 0;
        FP om0 = 0;
        if (om < om_log_min){
            iom = (unsigned int) (om / dom_linear);
            om0 = iom * dom_linear;
        }
        else {
            const FP om_pow = native_log10(om);
            iom = (unsigned int) ((om_pow - om_pow_min) / dom_pow);
            om0 = powr(10, om_pow_min + iom * dom_pow);
            iom += iom_log_min;
        }
        const FP dom = om - om0;
        const FP4 cw1 = cw[iom];
        const FP w = cw1.s0 * dom * dom * dom + cw1.s1 * dom * dom + cw1.s2 * dom + cw1.s3;
        res_loc[tid] += w * w * native_exp(- w * rho / w0);
    }
    barrier(CLK_LOCAL_MEM_FENCE);
    if (BlockSize >= 256) {
        if (tid < 128) res_loc[tid] += res_loc[128 + tid];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (BlockSize >= 128) {
        if (tid < 64) res_loc[tid] += res_loc[64 + tid];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (BlockSize >= 64) {
        if (tid < 32) res_loc[tid] += res_loc[32 + tid];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (BlockSize >= 32) {
        if (tid < 16) res_loc[tid] += res_loc[16 + tid];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (BlockSize >= 16) {
        if (tid < 8) res_loc[tid] += res_loc[8 + tid];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (tid < 4) res_loc[tid] += res_loc[4 + tid];
    barrier(CLK_LOCAL_MEM_FENCE);
    if (tid < 2) res_loc[tid] += res_loc[2 + tid];
    barrier(CLK_LOCAL_MEM_FENCE);
    if (!tid) res[get_group_id(0)] = dom_fine * c_a * t * (res_loc[0] + res_loc[1]) / (2 * PI * rho * rho * w0);
}